const dust2 = {
	id: 1,
	name: 'dust2',
	displayName: 'de_dust2',
	mapView: [
		{
			id: 1,
			imageUrl: '/maps/dust2.png',
			spot: [
				{
					id: 1,
					mapview: 'upper',
					y: 76,
					x: 68,
					video: [
						{
							id: 1,
							spotId: 1,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/CT_spawn_from_A_ramp.jpg',
							content: 'No scripts used, tried only on 64 tick servers, good smoke for CT spawn on dust 2.',
							spot: 'CT spawn from A ramp',
							videoUrl: '/videos/dust2_video_smoke/CT_spawn_from_A_ramp',
						}
					]
				},
				{
					id: 2,
					mapview: 'upper',
					y: 59.5,
					x: 48,
					video: [
						{
							id: 2,
							spotId: 2,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Easy_XBox_Smoke.jpg',
							content: 'Jump throw script used. A fairly easy smoke to line up and execute with very little practice. Great to push cat and not get seen by someone watching from mid.',
							spot: 'Easy XBox Smoke',
							videoUrl: '/videos/dust2_video_smoke/Easy_XBox_Smoke.mp4',
						},
						{
							id: 16,
							spotId: 2,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Xbox_from_Outside_Long.jpg',
							content: 'Easy and fairly consistent jump throw I just found to smoke off xbox from outside long. I didn&apos;t find I could do the t-spawn to xbox throw very consistently so I found this one instead. Go in the corner by the garage door outside long, and standing jump throw slightly above the left window shutter on the right window. Should land perfectly and let you push up cat without anyone shooting you from mid.',
							spot: 'Xbox from Outside Long',
							videoUrl: '/videos/dust2_video_smoke/Xbox_from_Outside_Long.mp4',
						},
						{
							id: 15,
							spotId: 2,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Xbox_from_Palm.jpg',
							content: 'EN: aim as shown in the video, run by saying in the head - one, two, and throw',
							spot: 'Xbox from Palm',
							videoUrl: '/videos/dust2_video_smoke/Xbox_from_Palm.mp4',
						},
						{
							id: 18,
							spotId: 2,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/T-spawn_to_xbox.jpg',
							content: 'This nade requires a jump throw &amp; was performed on a 128 tick server.',
							spot: 'T-spawn to xbox',
							videoUrl: '/videos/dust2_video_smoke/T-spawn_to_xbox.mp4',
						},
						{
							id: 21,
							spotId: 2,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Xbox_from_t-spawn.jpg',
							content: 'Xbox from t-spawn.',
							spot: 'Xbox from t-spawn',
							videoUrl: '/videos/dust2_video_smoke/Xbox_from_t-spawn.mp4',
						},
						{
							id: 22,
							spotId: 2,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Xbox_from_lower_dark.jpg',
							content: 'Simple little smoke for xbox if your lower dark and want to help out your buddies in middle.',
							spot: 'Xbox from lower dark',
							videoUrl: '/videos/dust2_video_smoke/Xbox_from_lower_dark.mp4',
						}
					]
				},
				{
					id: 3,
					mapview: 'upper',
					y: 66,
					x: 11.5,
					video: [
						{
							id: 3,
							spotId: 3,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Hole_to_Upper_Tunnels.jpg',
							content: 'A simple CT smoke to cover the Tunnels from outside of the site',
							spot: 'Hole to Upper Tunnels',
							videoUrl: '/videos/dust2_video_smoke/Hole_to_Upper_Tunnels.mp4',
						}
					]
				},
				{
					id: 4,
					mapview: 'upper',
					y: 70,
					x: 13,
					video: [
						{
							id: 4,
							spotId: 4,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Upper_Tunnel_to_B.jpg',
							content: 'A simple T smoke that covers popular angles on bombsite B',
							spot: 'Upper Tunnel to B',
							videoUrl: '/videos/dust2_video_smoke/Upper_Tunnel_to_B.mp4',
						}
					]
				},
				{
					id: 5,
					mapview: 'upper',
					y: 55,
					x: 83,
					video: [
						{
							id: 5,
							spotId: 5,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Smoke_from_T_Spawn_to_A_L.jpg',
							content: 'A fast way to smoke off that annoying corner (if you have good spawn). Works with 128 tick server',
							spot: 'Smoke from T Spawn to A L...',
							videoUrl: '/videos/dust2_video_smoke/Smoke_from_T_Spawn_to_A_L.mp4',
						},
						{
							id: 13,
							spotId: 5,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Long_corner_from_long_doors.jpg',
							content: 'EN: Prescribe in the console bind &quot;v&quot; &quot;-attack; -attack2; <br> after aim to the point shown in the video, with your thumb simultaneously press the jump key (space) and the V key.',
							spot: 'Long corner from long doors',
							videoUrl: '/videos/dust2_video_smoke/Long_corner_from_long_doors.mp4',
						},
						{
							id: 14,
							spotId: 5,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Long_corner_from_taxi.jpg',
							content: 'Fast smoke in &quot;start&quot;.',
							spot: 'Long corner from taxi',
							videoUrl: '/videos/dust2_video_smoke/Long_corner_from_taxi.mp4',
						}
					]
				},
				{
					id: 6,
					mapview: 'upper',
					y: 75,
					x: 81,
					video: [
						{
							id: 6,
							spotId: 6,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/A_Long_Doors_to_A_Long_Cross.jpg',
							content: 'Line up crosshairs with the left cylinder on the radio mast to the powerline. Perform a running throw, letting go once the crosshair passes the roof of the building to the right.',
							spot: 'A Long Doors to A Long Cross',
							videoUrl: '/videos/dust2_video_smoke/A_Long_Doors_to_A_Long_Cross.mp4',
						}
					]
				},
				{
					id: 7,
					mapview: 'upper',
					y: 74.5,
					x: 42,
					video: [
						{
							id: 7,
							spotId: 7,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Mid_to_B_from_Lower_tunnels.jpg',
							content: 'EN: Great to use close off mid AWP, on attack A from Short.',
							spot: 'Mid to B from Lower tunnels',
							videoUrl: '/videos/dust2_video_smoke/Mid_to_B_from_Lower_tunnels.mp4',
						},
						{
							id: 10,
							spotId: 7,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Mid_to_B_from_Lower_tunne.jpg',
							content: 'Safe mid-to-b smoke.',
							spot: 'Mid to B from Lower tunne...',
							videoUrl: '/videos/dust2_video_smoke/Mid_to_B_from_Lower_tunne.mp4',
						},
						{
							id: 11,
							spotId: 7,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Mid_to_B_from_Lower_tunne.jpg',
							content: 'EN: Great to use close off mid AWP, on attack A from Short.',
							spot: 'Mid to B from Lower tunne...',
							videoUrl: '/videos/dust2_video_smoke/Mid_to_B_from_Lower_tunne.mp4',
						}
					]
				},
				{
					id: 8,
					mapview: 'upper',
					y: 45,
					x: 69,
					video: [
						{
							id: 8,
							spotId: 8,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Long_doors_from_Long_car.jpg',
							content: 'EN: Prescribe in the console bind &quot;v&quot; &quot;-attack; -attack2; <br> after aim to the point shown in the video, with your thumb simultaneously press the jump key (space) and the V key.',
							spot: 'Long doors from Long car',
							videoUrl: '/videos/dust2_video_smoke/Long_doors_from_Long_car.mp4',
						}
					]
				},
				{
					id: 9,
					mapview: 'upper',
					y: 68,
					x: 11.5,
					video: [
						{
							id: 9,
							spotId: 9,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Outside_tunnel_from_Mid_to_B.jpg',
							content: 'EN: Prescribe in the console bind &quot;v&quot; &quot;-attack; -attack2; after aim to the point shown in the video, made one step and with your thumb simultaneously press the jump key (space) and the V key.',
							spot: 'Outside tunnel from Mid to B',
							videoUrl: '/videos/dust2_video_smoke/Outside_tunnel_from_Mid_to_B.mp4',
						}
					]
				},
				{
					id: 12,
					mapview: 'upper',
					y: 86.3,
					x: 26,
					video: [
						{
							id: 12,
							spotId: 12,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Window_from_outside_tunnel.jpg',
							content: 'EN: Great to use on B - clear..',
							spot: 'Window from outside tunnel',
							videoUrl: '/videos/dust2_video_smoke/Window_from_outside_tunnel.mp4',
						}
					]
				},
				{
					id: 17,
					mapview: 'upper',
					y: 74,
					x: 48,
					video: [
						{
							id: 17,
							spotId: 17,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Fake_mid_to_B.jpg',
							content: 'Could be used to fake mid-to-b or if a flanker wants to trow the smoke.',
							spot: 'Fake mid to B',
							videoUrl: '/videos/dust2_video_smoke/Fake_mid_to_B.mp4',
						},
						{
							id: 19,
							spotId: 17,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/Mid_to_b_from_xbox.jpg',
							content: 'Easy mid-to-b smoke.',
							spot: 'Mid to b from xbox',
							videoUrl: '/videos/dust2_video_smoke/Mid_to_b_from_xbox.mp4',
						}
					]
				},
				{
					id: 20,
					mapview: 'upper',
					y: 10,
					x: 10,
					video: [
						{
							id: 20,
							spotId: 20,
							thumbnail: '/thumbnails/dust2_thumbnails_smoke/CT-spawn_from_short.jpg',
							content: 'Simple little smoke to block of anyone in ct-spawn or a awp&apos;er watching from B.',
							spot: 'CT-spawn from short',
							videoUrl: '/videos/dust2_video_smoke/CT-spawn_from_short.mp4',
						}
					]
				},
			]
		}
	]
}

export const maps = [dust2]

export const users = [
    {
        id: 1, 
        username: 'monster', 
        password: 'admin123', 
        email: 'jordangauthier4@outlook.com',
        avatar: 'avatar.jpg'
    },
]

export const news = [
	{
		id: 1,
		content: 'Following the grand final, we talked to NiKo and heard about turning around FaZe after the initial struggle, the comeback against NiP on Nuke, and plans for the future',
		imgSrc: 'https://static.hltv.org/images/galleries/12002-medium/1572729595.2587.jpeg',
	},
	{
		id: 2,
		content: 'Following the grand final, we talked to NiKo and heard about turning around FaZe after the initial struggle, the comeback against NiP on Nuke, and plans for the future',
		imgSrc: 'https://static.hltv.org/images/galleries/12002-medium/1572729595.2587.jpeg',
	},
	{
		id: 3,
		content: 'Alibaba Sports, who is running WESG 2019-2020 in cooperation with U Can Company (UCC), have announced the schedule of the open and closed regional qualifiers which will kick off shortly, the first one starting on November 13.',
		imgSrc: 'https://static.hltv.org/images/galleries/12002-medium/1572729595.2587.jpeg',
	},
	{
		id: 4,
		content: 'Astralis had to fight for early control on the opening map after a pistol round went their way, as a big commotion on long saw them narrowly escape with a 3-0 lead, but from there on their double AWP setup starring Peter "dupreeh" Rasmussesection at middle and Nicolai "device" Reedtz on A had FaZe unable to find entry into either bombsite.',
		imgSrc: 'https://static.hltv.org/images/galleries/12008-medium/1573297312.8013.jpeg',
	},
	{
		id: 5,
		content: 'The tournament will span from November 11 - December 22, featuring three stages: the closed qualifier, main and final stages. The closed qualifier will see four participants, namely SKADE, Espada, Izako Boars and EXTREMUM duke it out in a GSL, best-of-sectionhree bracket on November 11-12 in looks to secure two slots at the main stage. The top two sides will join 14 invited teams in the Swiss, best-of-three main stage, running from November 13 - December 8.',
		imgSrc: 'https://static.hltv.org/images/galleries/11890-medium/1566599896.1495.jpeg',
	}
]
