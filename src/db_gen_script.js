const axios = require('axios');
const cheerio = require('cheerio');
var exec = require('child_process').exec, child

const url = 'http://www.csgonades.com/maps/dust2?nadetype=1';
const video_dir = 'dust2_video_smoke'
const thumbnails_dir = 'dust2_thumbnails_smoke'
const name = 'dust2';
const displayName = 'de_dust2';
const mapView = []
const JGDynamicMapUrl = '/maps/dust2.png'
const x = 1;
const y = 1;

var array = []
var videoId = 1;
var spotId = 1;
var id = 1;

//fetch the .mp4 video from fetch_video_and_content
async function fetch_video(ele) {
    console.log('fetching video and content for : ' + ele.title + ' url : ' + ele.url + ' in process...')
    await axios(ele.url).then(function (response) {
        const html = response.data;
        const $ = cheerio.load(html);
        ele.url = $('source').attr('src')
        console.log('complete')
    }).catch(error => {
        console.log("An error has occured")
        console.log(error)
    });
}

// fetch video url and content from initial data
async function fetch_video_and_content(ele) {
    console.log('fetching video and content for : ' + ele.title + ' url : http://www.csgonades.com' + ele.url + ' in process...')
    await axios('http://www.csgonades.com' + ele.url).then(function (response) {
        const html = response.data;
        const $ = cheerio.load(html);
        ele.url = $('iframe').attr('src')
        ele.content = $('.nade_desc p').html().split('\n').join(' ')
        console.log('complete')
    }).catch(error => {
        console.log("An error has occured")
        console.log(error)
    });

}

// fetch initial data (title, thumbnails, video url url)
async function fetch_initial_data() {
    console.log('fetch inital data (thumbnail and title and suffix url) from : ' + url)
    await axios(url).then(function (response) {
        const html = response.data;
        const $ = cheerio.load(html);
        $('.nade_link').each(function (i, elm) {
            array.push({
                url: $(this).attr('href'),
                title: $(this).find('.nadetitle').html(),
                imgUrl: $(this).find('img').attr('src')
            })
        });
        console.log('complete')
    }).catch(error => {
        console.log('An error has occured')
        console.log(error)
    });
}

// fetch all the data needed to create our structure
async function fetch() {
    await fetch_initial_data()
    for (let i = 0; i < array.length; i++)
        await fetch_video_and_content(array[i]);
    for (let i = 0; i < array.length; i++)
        await fetch_video(array[i])
}


// evaluate a bash command
function eval(command) {
    exec(command,
        function (error, stdout, stderr) {
        }
    );
}

function download() {
    array.forEach(ele => {
        const thumbnailNameFormatted = ele.title.split(' ').join('_') + ' ' + ele.imgUrl
        const videoNameFormatted = ele.title.split(' ').join('_') + ' ' + ele.url
        eval('wget -O  thumbnails/' + thumbnails_dir + '/' + thumbnailNameFormatted)
        eval('wget -O  videos/' + video_dir + '/' + videoNameFormatted)
    })
}

// download all thumbnails and video locally
async function run() {
    await fetch()
    eval('mkdir -p videos/' + video_dir)
    eval('mkdir -p thumbnails/' + thumbnails_dir)
    download()
    build_db_object()
}

function build_db_object() {
    const thumbnailNameFormatted = ele.title.split(' ').join('_')
    const videoNameFormatted = ele.title.split(' ').join('_')
    console.log('const ' + name + ' = {')
    console.log('	id: ' + id + ',')
    console.log('	name: ' + "\'" + name + "\',")
    console.log('	displayName: ' + "\'" + displayName + "\',")
    console.log('	mapView: [')
    console.log('		{')
    console.log('			imageUrl: ' + "\'" + JGDynamicMapUrl + "\',")
    console.log('			spot: [')
    array.forEach(ele => {
        console.log('				{')
        console.log('					id: ' + spotId + ',')
        console.log('					y: ' + y + ',')
        console.log('					x: ' + x + ',')
        console.log('					video: [')
        console.log('						{')
        console.log('							id: ' + videoId + ',')
        console.log('							spotId: ' + spotId + ',')
        console.log("							thumbnail: \'/thumbnails/" + thumbnails_dir + "/" + thumbnailNameFormatted + "\',")
        console.log('							content: ' + "\'" + ele.content + '\',')
        console.log('							spot: ' + "\'" + ele.title + '\',')
        console.log("							videoUrl: \'/videos/" + video_dir + '/' + videoNameFormatted + "\',")
        console.log('						}')
        console.log('					]')
        console.log('				},')
        videoId++;
        spotId++;
    })
    console.log('			]')
    console.log('		}')
    console.log('	]')
    console.log('}')
}

run()
