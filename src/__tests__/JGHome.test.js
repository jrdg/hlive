import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGHomeRouter from '../components/JGHome';
import jext from 'jest-extended';
import {MemoryRouter, BrowserRouter, Router} from 'react-router-dom'
import {createBrowserHistory} from 'history'

describe('JGHome snapshot', function() {
    it('check default snapshot', ()=>{
        const tree = shallow(<JGHomeRouter onClickClosePopup={() => {}}/>)
        expect(tree).toMatchSnapshot();
    })
});

describe('JGHome events',function() {
    it('closePopup should be called when click on the background of the popup', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const closePopup = jest.fn();
        const tree = mount(
            <MemoryRouter initialEntries={["/login"]}>
                <JGHomeRouter
                    onClickClosePopup={closePopup}
                    toggle='Login'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        tree.find('JGHome').find('.home_popup_background').simulate('click');
        expect(closePopup).toHaveBeenCalledTimes(1);
    })
})

describe('function test', function() {

    it('loginProcess should be called', () => {
        const mock = jest.fn();
        const tree = mount(
            <MemoryRouter>
                <JGHomeRouter 
                    loginProcess={mock} 
                    onClickClosePopup={() => {}}
                />
            </MemoryRouter>
        );
        tree.find('JGHome').instance().loginProcess();
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('closePopup should be called', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const closePopup = jest.fn();
        const tree = mount(
            <MemoryRouter>
                <JGHomeRouter
                    onClickClosePopup={closePopup}
                    toggle='Login'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        tree.find("JGHome").instance().closePopup()
        expect(closePopup).toHaveBeenCalledTimes(1);
    })

    it('popupDidMount should be called', () => {
        const popupDidMountMock = jest.fn();
        const history = createBrowserHistory();
        const tree = mount(
            <Router history={history}>
                <JGHomeRouter
                    onClickClosePopup={() => {}}
                    toggle='none'
                />
            </Router>
        )
        tree.find("JGHome").instance().popupDidMount = popupDidMountMock;
        history.push("/login")
        tree.setProps({children: (
            <JGHomeRouter
            onClickClosePopup={() => {}}
            toggle='Login'
        />
        )})
        expect(popupDidMountMock).toHaveBeenCalledTimes(1);
    })

    it('popupDidUnMount should be called', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const popupDidUnMountMock = jest.fn();
        const history = createBrowserHistory();
        const tree = mount(
            <Router history={history}>
                <JGHomeRouter
                    onClickClosePopup={() => {}}
                    toggle='none'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </Router>
        )
        tree.find("JGHome").instance().popupDidUnMount = popupDidUnMountMock;
        history.push("/login")
        tree.setProps({children: (
            <JGHomeRouter
            onClickClosePopup={() => {}}
            toggle='Login'
            logo_ref={logo_ref}
            nav_main_wrapper_ref={nav_main_wrapper_ref}
        />
        )})
        history.push("/none")
        tree.setProps({children: (
            <JGHomeRouter
            onClickClosePopup={() => {}}
            toggle='none'
            logo_ref={logo_ref}
            nav_main_wrapper_ref={nav_main_wrapper_ref}
        />
        )})
        expect(popupDidUnMountMock).toHaveBeenCalledTimes(1);
    })

    it('Check popupDidMount style modification on reference and document', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const tree = mount(
            <MemoryRouter initialEntries={["/login"]}>
                <JGHomeRouter
                    onClickClosePopup={() => {}}
                    toggle='Login'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        expect(logo_ref.current.style.marginLeft).toEqual('8px');
        expect(nav_main_wrapper_ref.current.style.paddingRight).toEqual('24px');
        expect(document.body.style.overflow).toEqual('hidden');
        expect(document.body.style.paddingRight).toEqual('16px');
    })

    it('Check popupDidUnMount style modification on reference and document', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const tree = mount(
            <MemoryRouter initialEntries={["/login"]}>
                <JGHomeRouter
                    onClickClosePopup={() => {}}
                    toggle='Login'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        tree.unmount();
        expect(logo_ref.current.style.marginLeft).toEqual('0px');
        expect(nav_main_wrapper_ref.current.style.paddingRight).toEqual('0px');
        expect(document.body.style.overflow).toEqual('visible');
        expect(document.body.style.paddingRight).toEqual('0px');
    })
})

describe('Chech reference', function() {
    it('Check if logo_ref reference is equal to the actual value pass to it as a parameter', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const tree = mount(
            <MemoryRouter>
                <JGHomeRouter 
                    onClickClosePopup={() => {}}
                    toggle='none'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        expect(tree.find('JGHome').props().logo_ref).toBe(logo_ref);
    })

    it('Check if nav_main_wrapper_ref reference is equal to the actual value pass to it as a parameter', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const tree = mount(
            <MemoryRouter>
                <JGHomeRouter 
                    onClickClosePopup={() => {}}
                    toggle='none'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        expect(tree.find("JGHome").props().nav_main_wrapper_ref).toBe(nav_main_wrapper_ref);
    })
})

describe('JGHome toggle different value toggle popup', function() {
    it('Check if toggle props with none value do not render the popup', () => {
        const tree = mount(
            <MemoryRouter>
                <JGHomeRouter 
                    onClickClosePopup={() => {}}
                    toggle='none'
                />
            </MemoryRouter>
        )
        const t = tree.find('JGPopup');
        expect(t.exists()).toEqual(false);
    })

    it('Check if toggle props with Login value render the popup', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const tree = mount(
            <MemoryRouter initialEntries={["/login"]}>
                <JGHomeRouter 
                    onClickClosePopup={() => {}}
                    toggle='Login'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        const t = tree.find('JGPopup');
        expect(t.exists()).toEqual(true);
    })

    it('Check if toggle props with SignIn value render the popup', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const tree = mount(
            <MemoryRouter initialEntries={["/signin"]}>
                <JGHomeRouter 
                    onClickClosePopup={() => {}}
                    toggle='SignIn'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        const t = tree.find('JGPopup');
        expect(t.exists()).toEqual(true);
    })
})

describe('JGHome props custom value', function() {
    it('Check if toggle props with Login value equal login', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const tree = mount(
            <MemoryRouter>
                <JGHomeRouter 
                    onClickClosePopup={() => {}}
                    toggle='Login'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        expect(tree.find('JGHome').props().toggle).toEqual('Login');
    })

    it('Check if toggle props with signin value equal signin', () => {
        const logo_ref = React.createRef();
        const nav_main_wrapper_ref = React.createRef();
        mount(<p ref={logo_ref}></p>)                   // fake ref
        mount(<p ref={nav_main_wrapper_ref}></p>)       // fake ref
        const tree = mount(
            <MemoryRouter>
                <JGHomeRouter 
                    onClickClosePopup={() => {}}
                    toggle='SignIn'
                    logo_ref={logo_ref}
                    nav_main_wrapper_ref={nav_main_wrapper_ref}
                />
            </MemoryRouter>
        )
        expect(tree.find("JGHome").props().toggle).toEqual('SignIn');
    })

    it('Check if toggle props with none value equal none', () => {
        const tree = mount(
            <MemoryRouter>
                <JGHomeRouter 
                    onClickClosePopup={() => {}}
                    toggle='none'
                />
            </MemoryRouter>
        )
        expect(tree.find("JGHome").props().toggle).toEqual('none');
    })
});