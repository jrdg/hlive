import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGLoginForm from '../components/JGLoginForm';
import jext from 'jest-extended';

describe('JGLoginForm events', function() {

    it('Check the onChange JGInput username state', () => {
        const mock = jest.fn();
        const tree = mount(<JGLoginForm loginProcess={mock}/>);
        tree.find('form').simulate('submit');
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('Check the onChange JGInput username state', () => {
        const tree = mount(<JGLoginForm/>);
        tree.find('JGInput')
            .at(0).find('input')
            .simulate('change',
                      {target: {value: 'jordan', 
                                name: 'username'}});
        expect(tree.state().username).toEqual('jordan');   
    })

    it('Check the onChange JGInput password state', () => {
        const tree = mount(<JGLoginForm/>);
        tree.find('JGInput')
            .at(1).find('input')
            .simulate('change',
                      {target: {value: 'jordan', 
                                name: 'password'}});
        expect(tree.state().password).toEqual('jordan');   
    })

    it('Check if handleOnSubmit is called onSubmit of the form', () => {
        const mock = jest.fn()
        const tree = mount(<JGLoginForm/>);
        tree.instance().handleSubmit = mock;
        tree.instance().forceUpdate();
        tree.find('form').simulate('submit');
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('Check if handleOnCHange is called onChange of JGInput', () => {
        const mock = jest.fn()
        const tree = mount(<JGLoginForm/>);
        tree.instance().handleOnChange = mock;
        tree.instance().forceUpdate();
        tree.find('JGInput').at(1).find('input').simulate('change')
        expect(mock).toHaveBeenCalledTimes(1);
    })
});

describe('JGLoginForm snapshot', function() {
    const tree = shallow(<JGLoginForm/>);
    expect(tree).toMatchSnapshot();
});

describe('JGLoginForm state', function() {

    it('Check the default username state', () => {
        const tree = mount(<JGLoginForm/>);
        const treeInstance = tree.instance();
        expect(treeInstance.state.username).toEqual('');
    })

    it('Check the default password state', () => {
        const tree = mount(<JGLoginForm/>);
        const treeInstance = tree.instance();
        expect(treeInstance.state.password).toEqual('');
    })
});

describe('JGLoginForm props', function() {
    it('Check the default username state', () => {
        const tree = mount(<JGLoginForm errMsg='test'/>);
        expect(tree.props().errMsg).toEqual('test');
    })
});