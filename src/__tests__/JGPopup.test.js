import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGPopup from '../components/JGPopup';
import jext from 'jest-extended';
import {BrowserRouter} from 'react-router-dom'

describe('JGPopup snapshot', function() {
    it('check default snapshot', ()=>{
        const tree = shallow(   
            <BrowserRouter>
                <JGPopup 
                    didMount={() => {}} 
                    didUnMount={() => {}}
                    onClickClosePopup={() => {}}
                    title=''
                >
                    <p></p>
                </JGPopup>
            </BrowserRouter>
        );
        expect(tree).toMatchSnapshot();
    })
});

describe('Events', function() {
    it('Check if closePopup inside function is called when click on the background', () => {
        const closePopupMock = jest.fn();
        const tree = mount(         
            <BrowserRouter>   
                <JGPopup 
                    didMount={() => {}} 
                    didUnMount={() => {}}
                    onClickClosePopup={closePopupMock}
                    title=''
                >
                    <p></p>
                </JGPopup>
            </BrowserRouter>
        )
        const comp = tree.find("JGPopup")
        comp.find('.home_popup_background').simulate('click');
        expect(closePopupMock).toHaveBeenCalledTimes(1);
    })
})

describe('JGPopup props custom value', function() {

    it('Check if didMount with a custom value is ok',() => {
        const mock =jest.fn();
        const tree = mount (
            <BrowserRouter>
                <JGPopup 
                    didMount={mock} 
                    didUnMount={() => {}}
                    onClickClosePopup={() => {}}
                    title=''
                >
                    <p></p>
                </JGPopup>
            </BrowserRouter>
        );
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('Check if didUnMount with a custom value is ok',() => {
        const mock =jest.fn();
        const tree = mount (
            <BrowserRouter>
                <JGPopup 
                    didMount={() => {}} 
                    didUnMount={mock}
                    onClickClosePopup={() => {}}
                    title=''
                >
                    <p></p>
                </JGPopup>
            </BrowserRouter>
        );
        tree.unmount();
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('Check if onClickClosePopup with a custom value is ok',() => {
        const mock =jest.fn();
        const tree = mount (
            <BrowserRouter>
                <JGPopup 
                    didMount={() => {}} 
                    didUnMount={() => {}}
                    onClickClosePopup={mock}
                    title=''
                >
                    <p></p>
                </JGPopup>
            </BrowserRouter>
        );
        tree.find('JGPopup').instance().closePopup();
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('Check if title with a custom value is ok',() => {
        const tree = mount (
            <BrowserRouter>
                <JGPopup 
                    didMount={() => {}} 
                    didUnMount={() => {}}
                    onClickClosePopup={() => {}}
                    title='test'
                >
                    <p></p>
                </JGPopup>
            </BrowserRouter>
        )
        expect(tree.find('JGPopup').props().title).toEqual('test');
    })

});