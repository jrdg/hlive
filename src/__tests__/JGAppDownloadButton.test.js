import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGAppDownloadButton from '../components/JGAppDownloadButton';
import jext from 'jest-extended';

describe('JGAppDownloadButton snapshot', function() {
    it('check if component render correctly', () => {
        const tree = shallow(<JGAppDownloadButton />)
        expect(tree).toMatchSnapshot();
    })
});

describe('JGAppDownloadButton props custom value', function() {
    it('check if height props with custom value is ok', () => {
        const tree = mount(<JGAppDownloadButton  height={144} />)
        expect(tree.props().height).toEqual(144);
    })

    it('check if width props with custom value is ok', () => {
        const tree = mount(<JGAppDownloadButton width={500} />)
        expect(tree.props().width).toEqual(500);
    })

    it('check if background image props with custom value is ok', () => {
        const tree = mount(<JGAppDownloadButton srcImg="/images/test.png"/>)
        expect(tree.props().srcImg).toEqual("/images/test.png");
    })
});

describe('JGAppDownloadButton proptype', function() {
    it('check if height props with custom value is ok', () => {
        const tree = mount(<JGAppDownloadButton  height={144} />)
        expect(tree.props().height).toBeNumber();
    })

    it('check if width props with custom value is ok', () => {
        const tree = mount(<JGAppDownloadButton width={500} />)
        expect(tree.props().width).toBeNumber();
    })

    it('check if background image props with custom value is ok', () => {
        const tree = mount(<JGAppDownloadButton srcImg="/images/test.png"/>)
        expect(tree.props().srcImg).toBeString();
    })
});