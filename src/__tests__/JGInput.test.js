import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGInput from '../components/JGInput';
import jext from 'jest-extended';

describe('JGInput events', function() {
    it('check the onChange callback', () => {  
        const mock = jest.fn(),
        props = {
            onChange: mock
        },
        tree = mount(<JGInput {...props} />);
        tree.find('input').simulate('change')
        expect(mock).toHaveBeenCalledTimes(1);
    });
});

describe('JGInput snapshot', function() {
    it('Render correctly JGInput', () => {
        const tree = shallow(<JGInput />)
        expect(tree).toMatchSnapshot();
    })
});

describe('JGInput props custom value', function() {

    it('check if custom errMsg is ok if errMsg isnt empty',() => {
        const tree = mount(<JGInput errMsg='test'/>);
        expect(tree.find('.JGInputErr').exists()).toEqual(true);
    })

    it('check if custom errMsg is ok if errMsg is empty',() => {
        const tree = mount(<JGInput errMsg=''/>);
        expect(tree.find('.JGInputErr').exists()).toEqual(false);
    })

    it('check the custom containerClassName prop', () => {  
        const props = {
            containterClassName: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().containterClassName).toEqual('test');
    });

    it('check the custom className prop', () => {  
        const props = {
            className: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().className).toEqual('test');
    });

    it('check the custom type prop', () => {  
        const props = {
            type: 'text',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().type).toEqual('text');
    });

    it('check the custom placeholder prop', () => {  
        const props = {
            placeholder: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().placeholder).toEqual('test');
    });

    it('check the custom value prop', () => {  
        const props = {
            value: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().value).toEqual('test');
    });

    it('check the custom abel prop', () => {  
        const props = {
            label: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().label).toEqual('test');
    });

    it('check the custom name prop', () => {  
        const props = {
            name: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().name).toEqual('test');
    });
});

describe('JGInput proptype', function() {
    it('check the containerClassName proptype', () => {  
        const props = {
            containterClassName: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().containterClassName).toBeString();
    });

    it('check the className proptype', () => {  
        const props = {
            className: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().className).toBeString();
    });

    it('check the type proptype', () => {  
        const props = {
            type: 'text',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().type).toBeString();
    });

    it('check the placeholder proptype', () => {  
        const props = {
            placeholder: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().placeholder).toBeString();
    });

    it('check the value proptype', () => {  
        const props = {
            value: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().value).toBeString();
    });

    it('check the label proptype', () => {  
        const props = {
            label: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().label).toBeString();
    });

    it('check the name proptype', () => {  
        const props = {
            name: 'test',
        }
        const tree = mount(<JGInput {...props} />)
        expect(tree.props().name).toBeString()
    });
});