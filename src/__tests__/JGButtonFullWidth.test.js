import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGButtonFullWidth from '../components/JGButtonFullWidth';
import jext from 'jest-extended';

describe('JGButtonFullWidth events', function() {

});

describe('JGButtonFullWidth snapshot', function() {
    it('Check if component render correctly',() => {
        const tree = shallow(<JGButtonFullWidth/>);
        expect(tree).toMatchSnapshot();
    })
});

describe('JGButtonFullWidth props custom value', function() {
    it('check if className props with custom value is ok', () => {
        const tree = mount(<JGButtonFullWidth  className="test" />)
        expect(tree.props().className).toEqual("test");
    })

    it('check if text props with custom value is ok', () => {
        const tree = mount(<JGButtonFullWidth text="test" />)
        expect(tree.props().text).toEqual("test");
    })
});

describe('JGButtonFullWidth proptype', function() {
    it('check if className props with custom value is ok', () => {
        const tree = mount(<JGButtonFullWidth  className="test" />)
        expect(tree.props().className).toBeString();
    })

    it('check if text props with custom value is ok', () => {
        const tree = mount(<JGButtonFullWidth text="test" />)
        expect(tree.props().text).toBeString();
    })
});