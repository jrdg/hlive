import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGHomeWhiteBannerTitle from '../components/JGHomeWhiteBannerTitle';
import jext from 'jest-extended';

describe('JGHomeWhiteBannerTitle snapshot', function() {
    it('Check if component render correctly',() => {
        const tree = shallow(<JGHomeWhiteBannerTitle/>);
        expect(tree).toMatchSnapshot();
    })
});

describe('JGHomeWhiteBannerTitle props custom value', function() {
    it('check if title props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle  title="test" />)
        expect(tree.props().title).toEqual("test")
    })

    it('check if firstSubTitle props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle firstSubTitle="test" />)
        expect(tree.props().firstSubTitle).toEqual("test");
    })

    it('check if secondSubTitle props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle secondSubTitle="test" />)
        expect(tree.props().secondSubTitle).toEqual("test");
    })

    it('check if top props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle  top={1} />)
        expect(tree.props().top).toEqual(1);
    })

    it('check if left props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle left={1} />)
        expect(tree.props().left).toEqual(1);
    })

    it('check if firstSubLeft props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle firstSubLeft={1} />)
        expect(tree.props().firstSubLeft).toEqual(1);
    })

    it('check if secondSubLeft props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle  secondSubLeft={1} />)
        expect(tree.props().secondSubLeft).toEqual(1);
    })

    it('check if firstSubWidth props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle firstSubWidth={1} />)
        expect(tree.props().firstSubWidth).toEqual(1);
    })

    it('check if secondSubWidth props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle secondSubWidth={1} />)
        expect(tree.props().secondSubWidth).toEqual(1);
    })
});

describe('JGButtonTogglePanel proptype', function() {
    it('check if title props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle  title="test" />)
        expect(tree.props().title).toBeString();
    })

    it('check if firstSubTitle props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle firstSubTitle="test" />)
        expect(tree.props().firstSubTitle).toBeString();
    })

    it('check if secondSubTitle props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle secondSubTitle="test" />)
        expect(tree.props().secondSubTitle).toBeString();
    })

    it('check if top props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle  top={1} />)
        expect(tree.props().top).toBeNumber();
    })

    it('check if left props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle left={1} />)
        expect(tree.props().left).toBeNumber();
    })

    it('check if firstSubLeft props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle firstSubLeft={1} />)
        expect(tree.props().firstSubLeft).toBeNumber();
    })

    it('check if secondSubLeft props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle  secondSubLeft={1} />)
        expect(tree.props().secondSubLeft).toBeNumber();
    })

    it('check if firstSubWidth props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle firstSubWidth={1} />)
        expect(tree.props().firstSubWidth).toBeNumber();
    })

    it('check if secondSubWidth props with custom value is ok', () => {
        const tree = mount(<JGHomeWhiteBannerTitle secondSubWidth={1} />)
        expect(tree.props().secondSubWidth).toBeNumber();
    })
});