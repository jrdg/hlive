import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGGrenadeList from '../components/JGGrenadeList';
import jext from 'jest-extended';
import { isTSAnyKeyword } from '@babel/types';

describe('JGGrenadeLis snapshot', function() {
    it('check default snapshot', () => {
        const tree = shallow(            
            <JGGrenadeList/>
        );
        expect(tree).toMatchSnapshot();
    })
});
