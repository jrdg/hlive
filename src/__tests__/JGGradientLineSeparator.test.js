import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGGradientLineSeparator from '../components/JGGradientLineSeparator';
import jext from 'jest-extended';

describe('JGGradientLineSeparator snapshot', function() {
    it('check if component render correctly', () => {
        const tree = shallow(<JGGradientLineSeparator />)
        expect(tree).toMatchSnapshot();
    })
});

describe('JGGradientLineSeparator props custom value', function() {
    it('check if height props with custom value is ok', () => {
        const tree = mount(<JGGradientLineSeparator height={4}/>)
        expect(tree.props().height).toEqual(4)
    })
});

describe('JGGradientLineSeparator proptype', function() {
    it('check if height proptype', () => {
        const tree = mount(<JGGradientLineSeparator height={4}/>)
        expect(tree.props().height).toBeNumber()
    })
});