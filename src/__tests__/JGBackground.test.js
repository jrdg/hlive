import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGBackground from '../components/JGBackGround';
import jext from 'jest-extended';

describe('JGBackground snapshot', function() {
    it('check default snapshot', ()=>{
        const tree = shallow(<JGBackground srcImg=''/>)
        expect(tree).toMatchSnapshot();
    })
});

describe('JGBackground props custom value', function() {

    it('Check if srcImg with a custom value is ok',() => {
        const tree = mount(
            <JGBackground srcImg='test' />
        );
        expect(tree.props().srcImg).toEqual('test');
    })
});