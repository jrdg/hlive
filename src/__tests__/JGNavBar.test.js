import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGNavBar from '../components/JGNavbar';
import JGHome from '../components/JGHome';
import SessionContext from '../Context/SessionContext'
import jext from 'jest-extended';
import PropTypes from 'prop-types'
import {BrowserRouter, MemoryRouter, Router} from 'react-router-dom'
import {createBrowserHistory} from 'history'


describe('JGNavBar snapshot', function() {
    it('Check if component render correctly',() => {
        const tree = shallow(<JGNavBar />);
        expect(tree).toMatchSnapshot();
    })
});

describe('JGNavBar props custom value', function() {
    it('check if onClickLogin props with custom value is ok', () => {
        const mock = jest.fn();
        const tree = mount( 
            <BrowserRouter>
                <JGNavBar  onClickLogin={mock} />
            </BrowserRouter>
        )
        expect(tree.find("JGNavbar").props().onClickLogin).toEqual(mock);
    })

    it('check if navGlobal props with custom value is ok', () => {
        const tree = mount(
            <BrowserRouter>
                <JGNavBar  navGlobal="test" />
            </BrowserRouter>
        )
        expect(tree.find("JGNavbar").props().navGlobal).toEqual("test");
    })

    it('check if midWrapper props with custom value is ok', () => {
        const tree = mount( 
            <BrowserRouter>
                <JGNavBar midWrapper="test" /> 
            </BrowserRouter>
        )
        expect(tree.find("JGNavbar").props().midWrapper).toEqual("test");
    })

    it('check if midWrapperSpecific props with custom value is ok', () => {
        const tree = mount(
            <BrowserRouter>
                <JGNavBar midWrapperSpecific="test" />
            </BrowserRouter>
        )
        expect(tree.find("JGNavbar").props().midWrapperSpecific).toEqual("test");
    })

    it('check if nav_main_wrapper_ref props with custom value is ok', () => {
        const rf = React.createRef();
        const tree = mount(
            <BrowserRouter>
                <JGNavBar nav_main_wrapper_ref={rf} />
            </BrowserRouter>
        )
        expect(tree.find("JGNavbar").props().nav_main_wrapper_ref).toBe(rf);
    })
});

describe('JGNavBar propstype', function() {

    it('check if onClickSignIn props with custom value is ok', () => {
        const mock = jest.fn();
        const tree = mount(
            <BrowserRouter>
                <JGNavBar onClickSignIn={mock} />
            </BrowserRouter>
        )
        expect(tree.find("JGNavbar").props().onClickSignIn).toBeFunction();
    })

    it('check if navGlobal propstype is ok', () => {
        const tree = mount(
            <BrowserRouter>
                <JGNavBar  navGlobal="test" />
            </BrowserRouter>
        )
        expect(tree.find("JGNavbar").props().navGlobal).toBeString();
    })

    it('check if midWrapper proptype', () => {
        const tree = mount(
            <BrowserRouter>
                <JGNavBar midWrapper="test" />
            </BrowserRouter>
        )
        expect(tree.find("JGNavbar").props().midWrapper).toBeString();
    })

    it('check if midWrapperSpecific proptype', () => {
        const tree = mount(
            <BrowserRouter>
                <JGNavBar midWrapperSpecific="test" />
            </BrowserRouter>
        )
        expect(tree.find("JGNavbar").props().midWrapperSpecific).toBeString();
    })
});

describe('JGNavbar events', function() {
    it('check the onClickLogin callback', () => {  
        const mock = jest.fn();
        const tree = mount(
            <BrowserRouter>
                <JGNavBar onClickLogin={mock} />
            </BrowserRouter>
        );
        tree.find('#LogInButton').simulate('click')
        expect(mock).toHaveBeenCalledTimes(1);
    });

    it('check the onClickSignin callback', () => {  
        const mock = jest.fn();
        const tree = mount(
            <BrowserRouter>
                <JGNavBar onClickSignIn={mock}/>
            </BrowserRouter>
        );
        tree.find('#SignInButton').simulate('click')
        expect(mock).toHaveBeenCalledTimes(1);
    });
});