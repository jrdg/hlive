import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGSectionOneHome from '../components/JGSectionOneHome';
import jext from 'jest-extended';

describe('JGSectionOneHome snapshot', function() {
    it('check default snapshot', ()=>{
        const tree = shallow(<JGSectionOneHome/>)
        expect(tree).toMatchSnapshot();
    })
});
