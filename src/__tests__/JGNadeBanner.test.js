import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGNadeBanner from '../components/JGNadeBanner';
import jext from 'jest-extended';

describe('JGNadeBanner snapshot', function() {
    it('Check if component render correctly',() => {
        const tree = shallow(<JGNadeBanner/>);
        expect(tree).toMatchSnapshot();
    })
});

describe('JGNadeBanner props custom value', function() {
    it('check if text props with custom value is ok', () => {
        const tree = mount(<JGNadeBanner  text="test" />)
        expect(tree.props().text).toEqual("test");
    })
});

describe('JGNadeBanner proptype', function() {
    it('check if text proptype', () => {
        const tree = mount(<JGNadeBanner  text="test" />)
        expect(tree.props().text).toBeString();
    })
});