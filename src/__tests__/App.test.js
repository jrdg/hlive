import React from 'react';
import { shallow, mount, render} from 'enzyme';
import { AppRouter } from '../components/App';
import jext from 'jest-extended';
import {BrowserRouter, MemoryRouter} from'react-router-dom'
import {createBrowserHistory} from 'history'

describe('App snapshot', function() {
    it('default snapshot', function() {
        const tree = shallow(    
        <BrowserRouter>
            <AppRouter/>
        </BrowserRouter>)
        expect(tree).toMatchSnapshot();
    })
});

describe('App events', function() {

    it('Check if loginProcess is called when submitting login form', () => {
        const mock = jest.fn();
        const history = createBrowserHistory()
        const tree = mount(
            <MemoryRouter initialEntries={["/login"]}>
                <AppRouter/>
            </MemoryRouter>
        )
        tree.find("App").instance().loginProcess = mock;
        tree.find("App").find('#LogInButton').simulate('click')
        tree.find("App").find('form').simulate('submit');
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('check if toggleLogin is called when click on login button', () => {
        const mock = jest.fn();
        const tree = mount(
            <BrowserRouter>
                <AppRouter/>
            </BrowserRouter>
        );
        tree.find("App").instance().toggleLogin = mock;
        tree.find('App').instance().forceUpdate()
        tree.find('#LogInButton').simulate('click')
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('check if url change to /login when click on login button', () => {
        const tree = mount(
            <BrowserRouter>
                <AppRouter/>
            </BrowserRouter>
        );
        tree.find('#LogInButton').simulate('click', {button: 0})
        expect(window.location.pathname).toEqual('/login')
    })

    
    it('check if url change to /signin when click on login button', () => {
        const tree = mount(
            <BrowserRouter>
                <AppRouter/>
            </BrowserRouter>
        );
        tree.find('#SignInButton').simulate('click', {button: 0})
        expect(window.location.pathname).toEqual('/signin')
    })

    it('check if url change to / when click on login button', () => {
        const tree = mount(
            <BrowserRouter>
                <AppRouter/>
            </BrowserRouter>
        );
        tree.find('.home_popup_background').simulate('click', {button: 0})
        expect(window.location.pathname).toEqual('/')
    })


    it('check if toggle state value is changed to Login when click on Login button', () => {
        const tree = mount(
            <BrowserRouter>
                <AppRouter/>
            </BrowserRouter>
        );
        tree.find('#LogInButton').simulate('click');
        expect(tree.find("App").state().toggle).toEqual('Login');
    })

    it('check if Login popup is rendered when click on Login Button', () => {
        const tree = mount(
            <BrowserRouter>
                <AppRouter/>
            </BrowserRouter>
        );
        tree.find('#LogInButton').simulate('click', {button: 0});
        expect(tree.find('JGLoginForm').exists()).toEqual(true);
    })

    it('check if toggleSignin is called when click on signin button', () => {
        const mock = jest.fn();
        const tree = mount(
            <BrowserRouter>
                <AppRouter/>
            </BrowserRouter>
        );
        tree.find("App").instance().toggleSignIn = mock;
        tree.find("App").instance().forceUpdate();
        tree.find('#SignInButton').simulate('click')
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('check if toggle state value is changed to SignIn when click on Signin button', () => {
        const tree = mount(
            <BrowserRouter>
                <AppRouter/>
            </BrowserRouter>
        );
        tree.find('#SignInButton').simulate('click');
        expect(tree.find("App").state().toggle).toEqual('SignIn');
    })

    it('check if Signin popup is rendered when click on Signin Button', () => {
        const tree = mount(
            <BrowserRouter>
                <AppRouter/>
            </BrowserRouter>
        );
        tree.find('#SignInButton').simulate('click', {button: 0});
        expect(tree.find('JGSigninForm').exists()).toEqual(true);
    })

    it('check if closePopup is called when click on the popup background', () => {
        const mock = jest.fn();
        const tree = mount(
            <MemoryRouter initialEntries={["/login"]}>
                <AppRouter/>
            </MemoryRouter>
        );
        tree.find("App").instance().closePopup = mock;
        tree.find("App").instance().forceUpdate();
        tree.find('.home_popup_background').simulate('click')
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('check if toggle state value is changed to none when click on jgpopup background', () => {
        const tree = mount(
            <MemoryRouter initialEntries={["/login"]}>
                <AppRouter toggle='Login'/>
            </MemoryRouter>
        );
        tree.find('.home_popup_background').simulate('click');
        expect(tree.find("App").state().toggle).toEqual('none');
    })

    it('check if JGpopup isnt rendered when click on the JGPopup background', () => {
        const tree = mount(
            <MemoryRouter initialEntries={["/login"]}>
                <AppRouter toggle='Login'/>
            </MemoryRouter>
        );
        tree.find('.home_popup_background').simulate('click');
        expect(tree.find('JGPopup').exists()).toEqual(false);
    })
})
