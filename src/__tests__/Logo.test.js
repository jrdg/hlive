import React from 'react';
import { shallow, mount, render, ref} from 'enzyme';
import Logo from '../components/Logo';
import jext from 'jest-extended';

describe('Logo snapshot', function() {
    it('check if component render correctly', () => {
        const tree = shallow(<Logo />)
        expect(tree).toMatchSnapshot();
    })
});

describe('Logo props custom value', function() {
    it('check if logo_ref props with custom value is ok', () => {
        const rf = React.createRef()
        const tree = mount(<Logo  logo_ref={rf}/>)
        expect(tree.props().logo_ref).toEqual(rf);
    })
});