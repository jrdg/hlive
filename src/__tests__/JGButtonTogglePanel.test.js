import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGButtonTogglePanel from '../components/JGButtonTogglePanel';
import jext from 'jest-extended';
import {BrowserRouter} from 'react-router-dom'

describe('JGButtonTogglePanel snapshot', function() {
    it('Check if component render correctly',() => {
        const tree = shallow(
            <BrowserRouter>
                <JGButtonTogglePanel link=''/>
            </BrowserRouter>
        );
        expect(tree).toMatchSnapshot();
    })
});

describe('JGButtonTogglePanel props custom value', function() {
    it('check if idName props with custom value is ok', () => {
        const tree = mount(
            <BrowserRouter>
                <JGButtonTogglePanel  idName="test" link='' />
            </BrowserRouter>
        )
        const comp = tree.find('JGButtonTogglePanel');
        expect(comp.props().idName).toEqual("test");
    })

    it('check if classStyle props with custom value is ok', () => {
        const tree = mount(
            <BrowserRouter>
                <JGButtonTogglePanel classStyle="test" link='' />
            </BrowserRouter>
        )
        const comp = tree.find('JGButtonTogglePanel');
        expect(comp.props().classStyle).toEqual("test");
    })

    it('check if text props with custom value is ok', () => {
        const tree = mount(
            <BrowserRouter>
                <JGButtonTogglePanel text="test" link=''/>
            </BrowserRouter>
        )
        const comp = tree.find('JGButtonTogglePanel');
        expect(comp.props().text).toEqual("test");
    })
});

describe('JGButtonTogglePanel proptype', function() {
    it('check if idName props with custom value is ok', () => {
        const tree = mount(
            <BrowserRouter>
                <JGButtonTogglePanel  idName="test" link=''/>
            </BrowserRouter>
        );
        const comp = tree.find('JGButtonTogglePanel');
        expect(comp.props().idName).toBeString();
    })

    it('check if classStyle props with custom value is ok', () => {
        const tree = mount(
            <BrowserRouter>
                <JGButtonTogglePanel classStyle="test" link=''/>
            </BrowserRouter>
        );
        const comp = tree.find('JGButtonTogglePanel');
        expect(comp.props().classStyle).toBeString();
    })

    it('check if text props with custom value is ok', () => {
        const tree = mount(
            <BrowserRouter>
                <JGButtonTogglePanel text="test" link=''/>
            </BrowserRouter>
        )
        const comp = tree.find('JGButtonTogglePanel');
        expect(comp.props().text).toBeString();
    })
});

describe('JGButtonTogglePanel events', function() {
    it('check the onClickLogin callback', () => {  
        const mock = jest.fn(),
        tree = mount(
            <BrowserRouter>
                <JGButtonTogglePanel onClick={mock} link=''/>
            </BrowserRouter>
        );
        const comp = tree.find('JGButtonTogglePanel');
        comp.find('button').simulate('click')
        expect(mock).toHaveBeenCalledTimes(1);
    });
});