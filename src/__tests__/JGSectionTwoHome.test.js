import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGSectionTwoHome from '../components/JGSectionOneHome';
import jext from 'jest-extended';

describe('JGSectionTwoHome snapshot', function() {
    it('check default snapshot', ()=>{
        const tree = shallow(<JGSectionTwoHome/>)
        expect(tree).toMatchSnapshot();
    })
});
