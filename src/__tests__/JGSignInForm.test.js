import React from 'react';
import { shallow, mount, render} from 'enzyme';
import JGSigninForm from '../components/JGSigninForm';
import jext from 'jest-extended';

describe('JGSigninForm events', function() {
    it('Check the onChange JGInput username state', () => {
        const tree = mount(<JGSigninForm/>);
        tree.find('JGInput')
            .at(0).find('input')
            .simulate('change',
                      {target: {value: 'jordan', 
                                name: 'signInUsername'}});
        expect(tree.state().signInUsername).toEqual('jordan');   
    })

    it('Check the onChange JGInput password state', () => {
        const tree = mount(<JGSigninForm/>);
        tree.find('JGInput')
            .at(1).find('input')
            .simulate('change',
                      {target: {value: 'jordan', 
                                name: 'signInPassword'}});
        expect(tree.state().signInPassword).toEqual('jordan');   
    })

    it('Check the onChange JGInput confirm password state', () => {
        const tree = mount(<JGSigninForm/>);
        tree.find('JGInput')
            .at(2).find('input')
            .simulate('change',
                      {target: {value: 'jordan', 
                                name: 'signInConfirmPassword'}});
        expect(tree.state().signInConfirmPassword).toEqual('jordan');   
    })

    it('Check the onChange JGInput email state', () => {
        const tree = mount(<JGSigninForm/>);
        tree.find('JGInput')
            .at(3).find('input')
            .simulate('change',
                      {target: {value: 'jordan', 
                                name: 'signInEmail'}});
        expect(tree.state().signInEmail).toEqual('jordan');   
    })

    it('Check the onChange JGInput confirm email state', () => {
        const tree = mount(<JGSigninForm/>);
        tree.find('JGInput')
            .at(4).find('input')
            .simulate('change',
                      {target: {value: 'jordan', 
                                name: 'signInConfirmEmail'}});
        expect(tree.state().signInConfirmEmail).toEqual('jordan');   
    })

    it('Check if handleOnSubmit is called onSubmit of the form', () => {
        const mock = jest.fn()
        const tree = mount(<JGSigninForm/>);
        tree.instance().handleSubmit = mock;
        tree.instance().forceUpdate();
        tree.find('form').simulate('submit');
        expect(mock).toHaveBeenCalledTimes(1);
    })

    it('Check if handleOnChange is called onChange of JGInput', () => {
        const mock = jest.fn()
        const tree = mount(<JGSigninForm/>);
        tree.instance().handleOnChange = mock;
        tree.instance().forceUpdate();
        tree.find('JGInput').at(1).find('input').simulate('change')
        expect(mock).toHaveBeenCalledTimes(1);
    })
});

describe('JGSigninForm snapshot', function() {
    const tree = shallow(<JGSigninForm/>);
    expect(tree).toMatchSnapshot();
});

describe('JGSigninForm state', function() {

    it('Check the default username state', () => {
        const tree = mount(<JGSigninForm />);
        const treeInstance = tree.instance();
        expect(treeInstance.state.signInUsername).toEqual('');
    })

    it('Check the default password state', () => {
        const tree = mount(<JGSigninForm />);
        const treeInstance = tree.instance();
        expect(treeInstance.state.signInPassword).toEqual('');
    })

    it('Check the default confirm password state', () => {
        const tree = mount(<JGSigninForm />);
        const treeInstance = tree.instance();
        expect(treeInstance.state.signInConfirmPassword).toEqual('');
    })

    it('Check the default email state', () => {
        const tree = mount(<JGSigninForm />);
        const treeInstance = tree.instance();
        expect(treeInstance.state.signInEmail).toEqual('');
    })

    it('Check the default confirm email state', () => {
        const tree = mount(<JGSigninForm />);
        const treeInstance = tree.instance();
        expect(treeInstance.state.signInConfirmEmail).toEqual('');
    })
});