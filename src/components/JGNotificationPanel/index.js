import React from 'react'
import {Link} from 'react-router-dom'
import './JGNotificationPanel.css'
import JGNotification from '../JGNotification'

function JGNotificationPanel(props) {

    function handleCloseNotification(event) {
        props.onClickNotification(event)
    }

    const ulStyle = {
        overflow: 'auto',
        maxHeight: '87%'
    }

    return (
        <React.Fragment>
            <h3 className='JGLoggedInSideBarSettingTitle'>
                <div id="JGLoggedInNotificationLiWrapper">
                    <p> Notifications </p>
                    <div onClick={handleCloseNotification}>
                        <i className="far fa-times-circle"></i>
                    </div>
                </div>
            </h3>
            <ul style={ulStyle}>
                <JGNotification/>
                <JGNotification/>
                <JGNotification/>
                <JGNotification/>
                <JGNotification/>
                <JGNotification/>
                <JGNotification/>
            </ul>
        </React.Fragment>
    )
}

export default JGNotificationPanel