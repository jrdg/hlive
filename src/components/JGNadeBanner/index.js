import React from 'react'
import PropTypes from 'prop-types'

import JGGradientLineSeparator from '../JGGradientLineSeparator'
import './JGNadeBanner.css'

function JGNadeBanner(props) {
    return (
      <section className='uniq_global_grenade_section'>
        <div className='frame_grenade'>
          <p>{props.text}</p>
        </div>
        <JGGradientLineSeparator/>
      </section>
    );
}

JGNadeBanner.defaultProps = {
  text: ''
}

JGNadeBanner.propTypes = {
  text: PropTypes.string.isRequired
}

export default JGNadeBanner