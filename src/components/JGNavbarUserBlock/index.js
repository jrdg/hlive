import React from 'react'
import {SessionContext} from '../../Context/SessionContext'
import JGButtonTogglePanel from '../JGButtonTogglePanel'
import './JGNavbarUserBlock.css'

export default function JGNavbarUserBlock(props) {
    const dir = '/images/'
    const replacement = 'data_image.jpeg'; 

    return (
        <SessionContext.Consumer>
            {({current_user}) => (
                <section id='JGNavbarUserBlockBackground'>
                    <img 
                        id='JGNavbarUserBlockAvatar' 
                        src={process.env.PUBLIC_URL + dir + (current_user.avatar != '' ? current_user.avatar : replacement)} 
                        alt='avatar'
                    />
                    <p id='JGNavbarUserBlockUsername' >{current_user.username}</p>
                    <JGButtonTogglePanel idName='JGNavbarUserOptionPanel' onClick={(e) => {props.onClickNotification(e)}} >
                        <i class="fas fa-bell"></i>
                    </JGButtonTogglePanel>
                    <JGButtonTogglePanel idName='JGNavbarUserOptionPanel' onClick={(e) => {props.onClickUserOptions(e)}} >
                        <i class="fas fa-ellipsis-v"></i>
                    </JGButtonTogglePanel>
                </section>
            )}
        </SessionContext.Consumer>
    )
}