import React, {useState} from 'react'
import {Link} from 'react-router-dom'
import './JGSidebarSection.css'
import {useTransition, animated} from 'react-spring'

export default function JGSidebarSection(props) {

    // state attribute that tell if the section is open or not
    const [open, setOpen] = useState(false);

    //all the class for the fontawsome arrow icon
    let className = 'fas fa-angle-right';

    //If the list is open then we add the class to rotate the arrow icon
    if(open) className += ' fa-rotate-90'

    const iconStyle = {
        marginRight: "10px", 
        fontSize: "8px", 
        color: props.active ? 'red' : '#404040'
    }

    return (
        props.singleButton ? 
                props.notSection ?
                    <Link to={props.link}>
                         <li onClick={props.onClick}>
                             <a className="JGSidebarSubA">
                                <i 
                                    class="fas fa-circle" 
                                    style={iconStyle}
                                />
                                {props.title}
                             </a>
                         </li>
                    </Link>
                    :
                    <Link to={props.link}>
                        <li>
                            <p className="JGSidebarTitle">
                                {props.title}
                            </p>
                        </li>
                    </Link>
             :
            <li>
                <p className="JGSidebarTitle" onClick={() => setOpen(!open)}>
                    <i className={className}/>
                    {props.title}
                </p>
                {open && <ul> {props.children} </ul>}
            </li>
    );
}