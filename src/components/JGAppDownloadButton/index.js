import React from 'react'
import './JGAppDownloadButton.css'
import PropTypes from 'prop-types';

function JGAppDownloadButton(props) {

    const buttonCss = {
        width: props.width + 'px',
        height: props.height + 'px',
        backgroundImage:"url("+ props.srcImg + ")"
    };

    return (
        <div>
            <button 
                className='JGAppDownloadButton' 
                style={buttonCss}
            >
            </button>
        </div>
    );
}

JGAppDownloadButton.defaultProps = {
    width: 400,
    height: 146,
    srcImg: ''
}

JGAppDownloadButton.propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    srcImg: PropTypes.string.isRequired
}

export default JGAppDownloadButton