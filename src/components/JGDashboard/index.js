import React from 'react'
import { withRouter } from 'react-router-dom'
import JGDashboardNew from '../JGDashboardNew'
import JGDashboardNewsBlock from '../JGDashboardNewsBlock'
import './JGDashboard.css'

function JGDashboard(props) {
    return (
        <section id="JGDashboardWrapper">
            <JGDashboardNewsBlock>
                <JGDashboardNew
                    content='Following the grand final, we talked to NiKo and heard about turning around FaZe after the initial struggle, the comeback against NiP on Nuke, and plans for the future.'
                    imgSrc='https://steamcdn-a.akamaihd.net/steam/subs/54029/header_586x192.jpg?t=1544227353'
                    className='JGPaddingBigPicture'
                    link='/news/1'
                />
            </JGDashboardNewsBlock>
            <JGDashboardNewsBlock>
                <JGDashboardNew
                    content='Following the grand final, we talked to NiKo and heard about turning around FaZe after the initial struggle, the comeback against NiP on Nuke, and plans for the future.'
                    imgSrc='https://static.hltv.org/images/galleries/12002-medium/1572729595.2587.jpeg'
                    className='JGPaddingLeft'
                    link='/news/1'
                />
                <JGDashboardNew
                    content=' Alibaba Sports, who is running WESG 2019-2020 in cooperation with U Can Company (UCC), have announced the schedule of the open and closed regional qualifiers which will kick off shortly, the first one starting on November 13.'
                    imgSrc='https://static.hltv.org/images/galleries/11694-medium/1552826824.3032.jpeg'
                    className='JGPaddingRight'
                    link='/news/3'
                />
            </JGDashboardNewsBlock>
            <JGDashboardNewsBlock>
                <JGDashboardNew
                    content='Astralis had to fight for early control on the opening map after a pistol round went their way, as a big commotion on long saw them narrowly escape with a 3-0 lead, but from there on their double AWP setup starring Peter "dupreeh" Rasmussesection at middle and Nicolai "device" Reedtz on A had FaZe unable to find entry into either bombsite.'
                    imgSrc='https://static.hltv.org/images/galleries/12008-medium/1573297312.8013.jpeg'
                    className='JGPaddingLeft'
                    link='/news/4'
                />
                <JGDashboardNew
                    content='The tournament will span from November 11 - December 22, featuring three stages: the closed qualifier, main and final stages. The closed qualifier will see four participants, namely SKADE, Espada, Izako Boars and EXTREMUM duke it out in a GSL, best-of-sectionhree bracket on November 11-12 in looks to secure two slots at the main stage. The top two sides will join 14 invited teams in the Swiss, best-of-three main stage, running from November 13 - December 8.'
                    imgSrc='https://static.hltv.org/images/galleries/11890-medium/1566599896.1495.jpeg'
                    className='JGPaddingRight'
                    link='/news/5'
                />
            </JGDashboardNewsBlock>
        </section>
    )
}

export const JGDashboardRouter = withRouter(JGDashboard);