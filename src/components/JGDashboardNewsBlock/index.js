import React, {useState, useEffect} from 'react'
import './JGDashboardNewsBlock.css'

export default function JGDashboardNewsBlock(props) {

    const [blockStyle, setBlockStyle] = useState({display: 'flex', flex: 1})

    return (
        <section style={blockStyle}>
            {props.children}
        </section>
    )
}
