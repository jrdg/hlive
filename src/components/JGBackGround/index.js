import React from 'react'
import PropTypes from 'prop-types'
import './JGBackground.css'

function JGBackground(props) {

    const specificStyle = {
        backgroundImage: 'url(' + props.srcImg + ')',
    };

    return (
        <div 
            style={specificStyle} 
            className='JGBackground'
        >
            {props.children}
        </div>
    );
}

JGBackground.defaultProps = {
    srcImg: ''
}

JGBackground.propTypes = {
    srcImg: PropTypes.string.isRequired,
}

export default JGBackground