import React, { useState } from 'react'
import { Link, useRouteMatch } from 'react-router-dom'
import { useSpring, animated } from 'react-spring'
import './JGPreVideoSection.css'

export default function JGPreVideoSection(props) {

    let { path, url } = useRouteMatch();

    function onClickOnVideo(event,){
        props.onClickOnVideo(event, props.video)
    }

    return (
            <article ref={props.reference} className='JGPreVideoSectionWrapper'>
                <Link to={{
                    pathname: url+'/spot/'+props.video.spotId+'/video/'+props.video.id,
                    state: {
                        videoList: props.list,
                        justRefresh: true
                    }
                }}>
                    <section onClick={onClickOnVideo} className='JGPreVideoSectionThumbailWrapper'>
                        <img className='JGPreVideoSectionThumbnail' src={props.video.thumbnail} />
                    </section>
                </Link>
                <section className='JGPreVideoSectionContentWrapper'>
                    <p className='JGPreVideoSectionContent' >{props.video.content}</p>
                    <p className='JGPreVideoSectionContentSpot' >{props.video.spot}</p>
                </section>
            </article>
    );
}
