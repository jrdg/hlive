import React from 'react'
import PropTypes from 'prop-types'
import './JGGrenadeList.css'

function JGGrenadeList(props) {
    return(
        <section id="nade_section">
            <div> {props.children} </div>
        </section>
    );
}

JGGrenadeList.propTypes = {
    children: (props, propName, componentName) => {
        let error;
        const prop = props[propName];
        React.Children.forEach(prop, (child) => {
            if (child.type.name !== 'JGNadeBanner') {
                error = new Error(`\`${componentName}\` 
                    only accepts children of type \`JGNadeBanner\`.`,);
            }
        });
        return error;
    },
}

export default JGGrenadeList