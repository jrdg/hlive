import React from 'react';

function Logo(props) {
    return (
            <section style={{flex:1}}>
                <img
                    ref={props.logo_ref}
                    id='logo' 
                    src='/images/hlive_logo.png' 
                    alt='Logo'
                />
            </section>
    );
}

export default Logo