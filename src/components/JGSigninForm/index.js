import React from 'react'
import JGInput from '../JGInput'
import JGButtonFullWidth from '../JGButtonFullWidth'

class JGSigninForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            signInUsername: '',
            signInPassword: '',
            signInConfirmPassword: '',
            signInEmail: '',
            signInConfirmEmail: ''
        }
    }

    handleOnChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
    }

    render() {

        const formStyle = {
            width: '400px'
        }

        return (
            <form style={formStyle} onSubmit={this.handleSubmit} method="post">
                <JGInput 
                    type="text"  
                    placeholder="Username"  
                    label="USERNAME" 
                    className='popup_input_text' 
                    containterClassName='popup_input_label_container'
                    value={this.state.signInUsername}
                    onChange={this.handleOnChange}
                    name='signInUsername'
                />
                <JGInput 
                    type="password"  
                    placeholder="Enter your password" 
                    label="PASSWORD" 
                    className='popup_input_text' 
                    containterClassName='popup_input_label_container'
                    value={this.state.signInPassword}
                    onChange={this.handleOnChange}
                    name='signInPassword'
                />
                <JGInput 
                    type="password"  
                    placeholder="Enter your password" 
                    label="CONFIRM PASSWORD" 
                    className='popup_input_text' 
                    containterClassName='popup_input_label_container'
                    value={this.state.signInConfirmPassword}
                    onChange={this.handleOnChange}
                    name='signInConfirmPassword'
                />
                <JGInput 
                    type="email"  
                    placeholder="Enter your email" 
                    label="EMAIL" 
                    className='popup_input_text' 
                    containterClassName='popup_input_label_container'
                    value={this.state.signInEmail}
                    onChange={this.handleOnChange}
                    name='signInEmail'
                />
                <JGInput 
                    type="email"  
                    placeholder="Enter your email" 
                    label="CONFIRM EMAIL" 
                    className='popup_input_text' 
                    containterClassName='popup_input_label_container'
                    value={this.state.signInConfirmEmail}
                    onChange={this.handleOnChange}
                    name='signInConfirmEmail'
                />
                <JGButtonFullWidth 
                    text='Create an account' 
                    className='popup_home_page_button'
                />
            </form>
        );
    }
}

export default JGSigninForm