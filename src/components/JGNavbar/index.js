import React from 'react';
import PropTypes from 'prop-types';
import Logo from '../Logo';
import JGButtonTogglePanel from '../JGButtonTogglePanel'
import './JGNavbar.css';
import '../JGuniversal.css'
import JGGradientLineSeparator from '../JGGradientLineSeparator'
import JGNavbarUserBlock from '../JGNavbarUserBlock'
import JGNotLoginMenu from '../JGNotLoginMenu'
import { SessionContext } from '../../Context/SessionContext'


/*
    @navGlobal               : This css class has the purpose to specify all the global content that isn't supposed
                               to place the content. Some of exemple of what you should place here :
                               (e.g : the background color of the nav bar , the position etc...)
    @prop midWrapper:          This is the css class that you pass to the block that will be 
                               the middle of the nav bar where all the content is supposed to be
    @prop midWrapperSpecific : it's like the midWrapper but it's purpose is to assign 
                               uniq css property to this block. By contrast to the midWrapper 
                               that is supposed to be used on a lot of component to center 
                               the content of your website.
*/

class JGNavbar extends React.Component {

    static contextType = SessionContext

    static defaultProps = {
        navGlobal: '',
        midWrapper: '',
        midWrapperSpecific: '',
        onClickLogin: () => {
            // unit testing
        },
        onClickSignIn: () => {
            // unit testing
        }
    }

    handleClickLogin = (event) => {
        this.props.onClickLogin(event);
    }

    handleClickSignIn = (event) => {
        this.props.onClickSignIn(event);
    }

    handleClickUserOptions = (event) => {
        this.props.onClickUserOptions(event)
    }

    handleClickNotification = (event) => {
        this.props.onClickNotification(event)
    }

    render() {
        return (
            <nav className={this.props.navGlobal}>
                <section
                    ref={this.props.nav_main_wrapper_ref}
                    className={this.props.midWrapper}
                    id={this.props.midWrapperSpecific}
                >
                    <Logo logo_ref={this.props.logo_ref} idName='nav_logo' />
                    { 
                      this.context.current_user == null ? 
                      <JGNotLoginMenu 
                        onClickLogin={this.handleClickLogin} 
                        onClickSignIn={this.handleClickSignIn}
                      /> : 
                      <JGNavbarUserBlock 
                        onClickUserOptions={this.handleClickUserOptions} 
                        onClickNotification={this.handleClickNotification} 
                      /> 
                    }
                </section>
                <JGGradientLineSeparator />
            </nav>
        );
    }
}

JGNavbar.propTypes = {
    onClickLogin: PropTypes.func.isRequired,
    onClickSignIn: PropTypes.func.isRequired,
    navGlobal: PropTypes.string,
    midWrapper: PropTypes.string,
    midWrapperSpecific: PropTypes.string,
}

export default JGNavbar