import React, {useState, useEffect} from 'react'
import './JGDynamicMapDot.css'

export default function JGDynamicMapDot(props) {

    const [active, setActive] = useState(false)
    const [bgColor, setBgColor] = useState('whitesmoke')
    
    const style = {
        left: props.x_axis + '%',
        bottom: props.y_axis + '%',
        backgroundColor: bgColor
    }

    useEffect(() => {
        setBgColor((active ? 'red' : 'whitesmoke'))
    }, [active])

    useEffect(() => {
        if(props.active){
            setActive(true)
        }
    }, [])

    function handleOnClick(){
        !active ? props.addVideoClick() :
                  props.removeVideoClick();
        setActive(!active)
    }
    
    return (
        <div onClick={() => handleOnClick()} className='JGDynamicMapDot' style={style}></div>
    )
}