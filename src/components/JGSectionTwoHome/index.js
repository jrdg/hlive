import React, { useEffect, useState } from 'react'
import JGBackground from '../JGBackGround';
import JGHomeWhiteBannerTitle from '../JGHomeWhiteBannerTitle'
import JGAppDownloadButton from '../JGAppDownloadButton'
import { Spring } from 'react-spring/renderprops'


function JGSectionTwoHome(props) {

  const [scrollY, setScrollY] = useState(0)
  const [animate, setAnimate] = useState(false)

  useEffect(() => {
    window.document.addEventListener('scroll', () => setScrollY(window.scrollY));
  }, [])

  useEffect(() => {
    if (scrollYHigherThanSectionTwo()) {
      setAnimate(true)
    }
  }, [window.scrollY])

  function scrollYHigherThanSectionTwo() {
    const container = document.getElementsByClassName('inner_background_glob')[1]
    return window.scrollY >=
          (container.offsetTop -
            (container.clientHeight +
              container.clientHeight *
              0.60
            )
          )
  }

  const fromStyle = {
    position: 'absolute',
    right: '-300px',
    opacity: 0
  }

  const toStyleNotAnimate = {
    position: 'absolute', 
    right: '-300px', 
    opacity: 0
  }

  const toStyleAnimate = {
    position: 'absolute', 
    right: '500px', 
    opacity: 1
  }

  return (
    <section>
      <JGBackground srcImg='/images/deuxieme_section.jpg'>
        <Spring
          from={fromStyle}
          to={
            animate ? toStyleAnimate :
                      toStyleNotAnimate
          }
        >
          {props =>
            <section style={props} className='inner_background_glob'>
              <JGHomeWhiteBannerTitle
                title='PLAY'
                firstSubTitle='ON OUR APP'
                secondSubTitle='BE BETTER'
                firstSubLeft={-100}
                secondSubWidth={400}
              />
              <JGAppDownloadButton
                srcImg='/images/apple.png'
              />
              <JGAppDownloadButton
                srcImg='/images/google.png'
              />
            </section>
          }
        </Spring>
      </JGBackground>
    </section>
  );
}

export default JGSectionTwoHome