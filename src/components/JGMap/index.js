import React, { useEffect, useState } from 'react'
import { useParams, withRouter, useHistory, Switch, Route, useRouteMatch, useLocation } from 'react-router-dom'
import { maps } from '../../Database/Db'
import JGVideoList from '../JGVideoList'
import JGDynamicMap from '../JGDynamicMap'
import JGDynamicMapDot from '../JGDynamicMapDot'
import './JGMap.css'

export default function JGMap(props) {

    let { map} = useParams();
    let history = useHistory();
    let location = useLocation()
    const [current_map, setMap] = useState('')
    const [videos, setVideos] = useState([])

    useEffect(() => {
        let i = 0;
        let find = false;
        setVideos([])
        while (!find && i < maps.length) {
            if (maps[i].name === map) {
                find = true;
                setMap(maps[i])
            }
            i++;
        }
        if (!find) {
            history.replace('/')
        }
    }, [map])

    useEffect(() => {
       if(props.location.state != undefined &&
          props.location.state.videoList != undefined &&
          props.location.state.videoList.length > 0) {
            setVideos(props.location.state.videoList)
        }
    }, [])

    function addVideoToList(spot) {
        setVideos([...spot.video, ...videos])
    }

    function removeVideoFromList(spot) {
        let copy_video = [];
        videos.forEach(video => {
            if (video.spotId !== spot.id)
                copy_video.push(video)
        })
        setVideos(copy_video)
    }

    function onClickOnVideo(event, current_video) {
        //setToggleVideo(true)
        //setCurrentVideo(current_video)
    }

    function dotShouldBeActiveOnRefresh(spotid) {
        let find = false
        let i = 0;
        while(i < videos.length && !find) {
            if(videos[i].spotId == spotid){
                find = true
            }
            i++;
        }
        return find
    }

    return (
        <section id="JGMap">
            {
                current_map !== '' &&
                <section className='JGMapWrapper'>
                    <section className='JGMapDynamicMapWrapper'>
                        {current_map.mapView.map((view, index) => (
                            <JGDynamicMap key={index} mapUrl={view.imageUrl}>
                                {view.spot.map((spot, index) =>
                                    <JGDynamicMapDot
                                        active={dotShouldBeActiveOnRefresh(spot.id)}
                                        addVideoClick={() => addVideoToList(spot)}
                                        removeVideoClick={() => removeVideoFromList(spot)}
                                        key={index}
                                        y_axis={spot.y}
                                        x_axis={spot.x}
                                    />
                                )}     
                            </JGDynamicMap>
                        ))}
                    </section>
                    <JGVideoList 
                        noAnim={location.state != undefined && location.state.justRefresh}
                        onClickOnVideo={onClickOnVideo} 
                        list={videos}
                        title={current_map.name} 
                    />
                </section>
            }
        </section>
    )
}

export const JGMapRouter = withRouter(JGMap)