import React from 'react'
import PropTypes from 'prop-types'
import JGGradientLineSeparator from '../JGGradientLineSeparator'
import './JGPopup.css'
import { Spring, Transition } from 'react-spring/renderprops'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    withRouter
} from "react-router-dom";


/**
 * for the height prop you msut give the height of the div that contain the
 * popup_input_container class 
 */
class JGPopup extends React.Component {

    static defaultProps = {
        title: 'POPUP',
        link: ''
    }

    constructor(props) {
        super(props)
        this.containerRef = React.createRef()
        this.state = {
            show: true,
            height: 0,
            heightAddition: 0,
        }
    }

    componentDidMount = () => {
        this.props.didMount();
        this.setState({
            heightAddition: this.props.noTitle ? 0 : 51,
        })
    }

    componentWillUnmount = () => {
        this.props.didUnMount();
    }

    closePopup = (event) => {
        this.setState({ show: false })
        this.props.onClickClosePopup(event);
    }

    render() {
        
        let finalHeight = this.props.height + this.state.heightAddition;
        let inputContainerStyle = { padding: this.props.noInnerPadding ? '0px' : '50px', }
        let formikStyle = {
            width: this.props.width + 'px',
            minHeight: finalHeight + 'px',
            height: this.props.fixedHeight ? finalHeight + 'px' : 'auto',
            marginLeft: -1 * this.props.width / 2 + 'px',
            marginTop: -1 * finalHeight / 2 + 'px',
        }

        return (
            <section>
                {
                    this.state.show &&
                    <section
                        className='home_popup_background'
                        onClick={this.closePopup}
                    >
                    </section>
                }
                <Transition
                    items={this.state.show}
                    config={{duration: 200}}
                    from={{ opacity: 0, top: "0%" }}
                    enter={{ opacity: 1, top: '50%' }}
                    leave={{ opacity: 0, top: "-30%" }}
                    onDestroyed={() => {this.props.history.push(this.props.link)}}
                >
                    {show => show && (
                        styles =>
                            <section ref={this.testRef} style={{ ...styles, ...formikStyle }} className='formik'>
                                {!this.props.noTitle && <h2>{this.props.title}</h2>}
                                <section ref={this.containerRef} style={inputContainerStyle} className='popup_input_container'>
                                    {this.props.children}
                                </section>
                                <JGGradientLineSeparator top={this.props.gtop} />
                            </section>)
                    }
                </Transition>
            </section>
        );
    }
}

JGPopup.propTypes = {
    didMount: PropTypes.func.isRequired,
    didUnMount: PropTypes.func.isRequired,
    onClickClosePopup: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    children: (props, propName, componentName) => {
        if (React.Children.count(props.children) === 0) {
            return new Error(
                componentName + ' need at least 1 child.'
            );
        }
    }
}

export default JGPopup

export const JGPopupRouter = withRouter(JGPopup);
