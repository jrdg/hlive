import React from 'react'
import './JGSliderPanel.css'
import PropTypes from 'prop-types';
import './JGSliderPanel.css'

export default function JGSliderPanel(props) {
    return (
        <section id={props.id} className="JGSliderPanelWrapper" style={{overflow: "visible"}}>    
            {props.children}
        </section>
    );
}

