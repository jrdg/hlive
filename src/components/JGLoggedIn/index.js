import React, { useState, useEffect } from 'react'
import { JGSidebarRouter } from '../JGSidebar'
import { JGDashboardRouter } from '../JGDashboard'
import { JGMapRouter } from '../JGMap'
import { useParams, withRouter, useHistory, Switch, Route, useRouteMatch, useLocation } from 'react-router-dom'
import JGSliderPanel from '../JGSliderPanel'
import { useTransition, animated } from 'react-spring'
import { Transition } from 'react-spring/renderprops'
import JGSidebarSection from '../JGSidebarSection'
import { Link } from 'react-router-dom'
import './JGLoggedIn.css'
import JGUserSettingPanel from '../JGUserSettingPanel'
import JGNotificationPanel from '../JGNotificationPanel'
import JGNew from '../JGNew'
import { JGPopupRouter } from '../JGPopup'
import { maps } from '../../Database/Db'

export default function JGLoggedin(props) {
    let location = useLocation()
    let { path, url } = useRouteMatch()
    let history = useHistory();
    const [videos, setVideos] = useState([])
    const [justRefresh, setJustRefresh] = useState(false)

    const userOptionTransition = useTransition(props.settingToggle, null, {
        from: { position: "fixed", right: "-300px" },
        enter: { right: "300px" },
        leave: { right: "-300px" },
    })

    const notificationTransition = useTransition(props.notificationToggle, null, {
        from: { position: "fixed", right: "-300px" },
        enter: { right: "300px" },
        leave: { right: "-300px" },
    })

    const pageTransition = useTransition(history.location, location => location.pathname, {
        immediate: () => location.state != undefined && location.state.justRefresh,
        from: { position: 'absolute', opacity: 0, width: '100%' },
        enter: { opacity: 1 },
        leave: { opacity: 1 }
    })

    function handleCloseUserSetting(event) {
        props.onClickUserOptions(event)
    }

    function handleCloseNotification(event) {
        props.onClickNotification(event)
    }

    return (
        <section>
            {
                pageTransition.map(({ item, props, key }) => (
                    <animated.div key={key} style={props}>
                        <Switch location={item}>
                            <Route exact path="/">
                                <JGDashboardRouter />
                            </Route>
                            <Route exact path="/news/:newId">
                                <JGNew />
                            </Route>
                            <Route path="/map/:map">
                                <JGMapRouter/>
                            </Route>
                        </Switch>
                    </animated.div>
                ))
            }

            <JGSidebarRouter />
            {
                userOptionTransition.map(({ item, key, props }) => item &&
                    <animated.div className='JGLoggedInSliderPanel' key={key} style={props}>
                        <JGSliderPanel id="UserSettingPanel">
                            <JGUserSettingPanel onClickUserOptions={handleCloseUserSetting} />
                        </JGSliderPanel>
                    </animated.div>)
            }
            {
                notificationTransition.map(({ item, key, props }) => item &&
                    <animated.div className='JGLoggedInSliderPanel' key={key} style={props}>
                        <JGSliderPanel id="notificationPanel">
                            <JGNotificationPanel onClickNotification={handleCloseNotification} />
                        </JGSliderPanel>
                    </animated.div>)
            }
            <Route path={`/map/:map/spot/:spotid/video/:videoid`}>
                <JGMapPopup />
            </Route>
        </section>
    )
}

export const JGLoggedInRouter = withRouter(JGLoggedin);


function JGMapPopup(props) {


    let { spotid, map, videoid } = useParams()
    const [current_video, setCurrent_video] = useState({})
    let location = useLocation()

    useEffect(() => {
        let find = false;
        for (let i = 0; i < maps.length; i++) {
            if (maps[i].name === map) {
                for (let j = 0; j < maps[i].mapView.length; j++) {
                    for (let k = 0; k < maps[i].mapView[j].spot.length; k++) {
                        if (maps[i].mapView[j].spot[k].id === parseInt(spotid)) {
                            for (let l = 0; l < maps[i].mapView[j].spot[k].video.length; l++) {
                                if (maps[i].mapView[j].spot[k].video[l].id === parseInt(videoid)) {
                                    setCurrent_video(maps[i].mapView[j].spot[k].video[l])
                                }
                            }
                        }
                    }
                }
            }
        }
    }, [])

    return (
        <JGPopupRouter
            gtop={1.9}
            width={700}
            height={393.75}
            noInnerPadding
            noTitle
            fixedHeight
            didMount={() => {}}
            didUnMount={() => {}}
            onClickClosePopup={() => {}}
            link={{
                pathname: '/map/' + map,
                state: {
                    videoList: location.state != undefined && location.state.videoList,
                    justRefresh: true
                }
            }}
        >
            <video width='700px' controls autoPlay>
                <source src={current_video.videoUrl} type="video/mp4" />
            </video>
        </JGPopupRouter>
    )
}