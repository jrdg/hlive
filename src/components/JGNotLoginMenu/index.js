import React from 'react';
import PropTypes from 'prop-types';
import JGButtonTogglePanel from '../JGButtonTogglePanel'
import './JGNotLoginMenu.css'


class JGNotLoginMenu extends React.Component {

    handleClickLogin = (event) => {
        this.props.onClickLogin(event);
    }

    handleClickSignIn = (event) => {
        this.props.onClickSignIn(event);
    }

    render() {
        return (
            <ul className="nav_link_list">
                <li><JGButtonTogglePanel
                    idName='SignInButton'
                    classStyle='nav_link_button'
                    text='Create an account'
                    onClick={this.handleClickSignIn}
                    link='/signin'
                />
                </li>
                <li><JGButtonTogglePanel
                    idName='LogInButton'
                    classStyle='nav_link_button'
                    text='Login'
                    onClick={this.handleClickLogin}
                    link='/login'
                />
                </li>
            </ul>
        );
    }
}

JGNotLoginMenu.propTypes = {
    onClickLogin: PropTypes.func.isRequired,
    onClickSignIn: PropTypes.func.isRequired,
}

export default JGNotLoginMenu