import React from 'react'
import PropTypes from 'prop-types';
import './JGHomeWhiteBannerTitle.css'

function JGHomeWhiteBannerTitle(props) {

    const topContainerStyle = {
        top: props.top,
        left: props.left
    }

    const firstTitleStyle = {
        width: props.firstSubWidth,
        left: props.firstSubLeft
    }

    const secondTitleStyle = {
        width: props.secondSubWidth,
        left: props.secondSubLeft
    }

    const topContainerClass = 'JGHomeWhiteBannerTitle_glob'
    const titleContainerClass = 'JGHomeWhiteBannerTitle_title'
    const firstTitleClass = 'JGHomeWhiteBannerTitle_firstsubtitle'
    const secondTitleClass = 'JGHomeWhiteBannerTitle_secondsubtitle'

    return (
        <section className={topContainerClass} style={topContainerStyle} >
            <section className={titleContainerClass}>
                <section>
                    <p>{props.title}</p>
                    <p className={firstTitleClass} style={firstTitleStyle}>
                        {props.firstSubTitle}
                    </p>
                    <p className={secondTitleClass} style={secondTitleStyle}>
                        {props.secondSubTitle}
                    </p>
                </section>
            </section>
        </section>
    );
}

JGHomeWhiteBannerTitle.defaultProps = {
    title: '',
    firstSubTitle: '',
    secondSubTitle: '',
}

JGHomeWhiteBannerTitle.propTypes = {
    title: PropTypes.string.isRequired,
    firstSubTitle: PropTypes.string.isRequired,
    secondSubTitle: PropTypes.string.isRequired,
    top: PropTypes.number,
    left: PropTypes.number,
    firstSubLeft: PropTypes.number,
    secondSubLeft: PropTypes.number,
    firstSubWidth: PropTypes.number,
    secondSubWidth: PropTypes.number
}

export default JGHomeWhiteBannerTitle