import React from 'react'
import JGBackground from '../JGBackGround';
import JGHomeWhiteBannerTitle from '../JGHomeWhiteBannerTitle'
import JGNadeBanner from '../JGNadeBanner'
import JGGrenadeList from '../JGGrenadeList';
import { Spring } from 'react-spring/renderprops'

function JGSectionOneHome(props) {

  const fromStyle = {
    position: 'absolute',
    right: '-300px',
    opacity: 0
  }

  const toStyle = {
    opacity: 1,
    right: '500px'
  }

  return (
    <section>
      <JGBackground srcImg='/images/premiere_section.jpg'>
        <Spring
          from={fromStyle}
          to={toStyle}
        >
          {props =>
            <section style={props} className='inner_background_glob'>
              <JGHomeWhiteBannerTitle
                title='THE BEST PLACE TO'
                firstSubTitle='KNOW IT ALL'
                secondSubTitle='NOT FORGETTING'
              />
              <JGGrenadeList>
                <JGNadeBanner text='MOLOTOV' />
                <JGNadeBanner text='HE' />
                <JGNadeBanner text='FLASHBANG' />
                <JGNadeBanner text='SMOKE' />
              </JGGrenadeList>
            </section>
          }
        </Spring>
      </JGBackground>
    </section>
  );
}

export default JGSectionOneHome