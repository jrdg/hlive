import React from 'react'
import PropTypes from 'prop-types';
import './JGButtonFullWidth.css'

function JGButtonFullWidth(props) {
    return (
        <div className='JGButtonFullWidthContainer'>
            <input 
                className={props.className}
                type="submit"
                value={props.text}
            />
        </div>
    );
}

JGButtonFullWidth.defaultProps = {
    className:'',
    text:'Submit'
}

JGButtonFullWidth.propTypes = {
    className: PropTypes.string,
    text: PropTypes.string
}

export default JGButtonFullWidth
