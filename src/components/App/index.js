import React from 'react';
import './App.css';
import '../JGuniversal.css';
import JGNavbar from '../JGNavbar'
import { JGHomeRouter } from '../JGHome';
import { users } from '../../Database/Db'
import { SessionContext } from '../../Context/SessionContext'
import {
  Route,
  withRouter,
} from "react-router-dom";
import {JGLoggedInRouter} from '../JGLoggedIn';

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      toggle: 'none',
      current_user: null,//users[0], // set it to null to disconnect
      loginErrMsg: '',
      settingOptionToggle: false,
      notificationToggle: false
    }
    this.nav_main_wrapper_ref = React.createRef();
    this.logo_ref = React.createRef();
  }

  toggleLogin = (event) => {
    this.setState({
      toggle: 'Login',
      loginErrMsg: ''
    });
  }

  toggleSignIn = (event) => {
    this.setState({
      toggle: 'SignIn',
    });
  }

  toggleUserOptions = (event) => {
    console.log("on toggle le user option panel")
    this.setState({
      settingOptionToggle: !this.state.settingOptionToggle,
      notificationToggle: false
    });
  }

  toggleNotification = (event) => {
    this.setState({
      notificationToggle: !this.state.notificationToggle,
      settingOptionToggle: false
    });
  }

  loginProcess = (event, username, password) => {
    let find = false;
    users.forEach(user => {
      if (user.username === username &&
        user.password === password) {
        this.setState({
          current_user: user
        });
        find = true;
      }
    })
    if(!find){
      this.setState({
        loginErrMsg: 'Wrong username or password please try again.'
      })
    } else{
      this.setState({loginErrMsg: '', toggle: 'none'})
      this.props.history.push('/')
    }
  }

  componentDidMount(){
      this.setPopulUrl()
  }

  setPopulUrl = () => {
    let tog = 'none'
    if(this.props.history.location.pathname === '/login'){
      tog = 'Login'
    }else if(this.props.history.location.pathname === '/signin'){
      tog = 'SignIn'
    }
    this.setState({
      toggle: tog
    })
  }

  render() {
    return (
        <SessionContext.Provider value={{ current_user: this.state.current_user }}>
          <div>
            <JGNavbar
              nav_main_wrapper_ref={this.nav_main_wrapper_ref}
              logo_ref={this.logo_ref}
              navGlobal='nav_main'
              midWrapperSpecific='nav_main_wrapper'
              midWrapper='main_wrapper'
              onClickLogin={this.toggleLogin}
              onClickSignIn={this.toggleSignIn}
              onClickUserOptions={this.toggleUserOptions}
              onClickNotification={this.toggleNotification}
            />
            <Route path="/">
              <SessionContext.Consumer>
                  {({ current_user }) => (
                      current_user === null ?
                        <JGHomeRouter
                          toggle={this.state.toggle}
                          onClickClosePopup={() => {}}
                          nav_main_wrapper_ref={this.nav_main_wrapper_ref}
                          logo_ref={this.logo_ref}
                          loginProcess={this.loginProcess}
                          loginErrMsg={this.state.loginErrMsg}
                        />
                        :
                        <div>
                            <JGLoggedInRouter
                              settingToggle={this.state.settingOptionToggle}
                              notificationToggle={this.state.notificationToggle}
                              onClickUserOptions={this.toggleUserOptions}
                              onClickNotification={this.toggleNotification}
                            />
                        </div>
                    )
                  }
              </SessionContext.Consumer>
            </Route>
          </div>
        </SessionContext.Provider>
    );
  }
}

export const AppRouter = withRouter(App);
