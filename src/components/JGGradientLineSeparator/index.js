import React from 'react';
import PropTypes from 'prop-types'
import './JGGradientLineSeparator.css'

function JGGradientLineSeparator(props) {

    const style = {
        height: props.height + 'px',
        marginTop: -1 * props.top+'px'
    }

    return (
        <div 
            className='JGGradientLineSeparator' 
            style={style}
        >
        </div>
    );
}

JGGradientLineSeparator.defaultProps = {
    height: 1
}

JGGradientLineSeparator.propTypes = {
    height: PropTypes.number
}

export default JGGradientLineSeparator