import React from 'react'
import {Link} from 'react-router-dom'
import './JGUserSettingPanel.css'

function JGUserSettingPanel(props) {

    function handleCloseUserSetting(event) {
        props.onClickUserOptions(event)
    }

    return (
        <React.Fragment>
            <h3 className='JGLoggedInSideBarSettingTitle'>
                <div id="JGLoggedInUserSettingLiWrapper">
                    <p> User's setting </p>
                    <div onClick={handleCloseUserSetting}>
                        <i className="far fa-times-circle"></i>
                    </div>
                </div>
            </h3>
            <ul>
                <Link to='/Setting'><li> Setting </li></Link>
                <Link to='/Logout'><li> Logout </li></Link>
            </ul>
        </React.Fragment>
    )
}

export default JGUserSettingPanel