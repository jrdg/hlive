import React, { useState } from 'react'
import { useTransition, animated } from 'react-spring'
import { Transition } from 'react-spring/renderprops'
import { Link } from 'react-router-dom'
import './JGDashboardNew.css'

export default function JGDashboardNew(props) {

    const [toggle, setToggle] = useState(false)

    function handleMouseEnter(event) {
        setToggle(true)
    }

    function handleMouseLeave(event) {
        setToggle(false)
    }

    const fromStyle = {
        height: '30%',
        opacity: 0
    }

    const enterStyle = {
        height: '30%',
        opacity: 0.8
    }

    const leaveStyle = {
        height: '30%',
        opacity: 0
    }

    return (
        <section onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave} style={props.style} className='JGDashboardNewGlobalWrapper'>
            <Link to={props.link}>
                <section className='JGDashboardNewWrapper'>
                    <img className='JGDashboardNewImg' src={props.imgSrc} />
                    <Transition
                        items={toggle}
                        from={fromStyle}
                        enter={enterStyle}
                        leave={leaveStyle}>
                        {toggle => toggle && (styles =>
                            <section style={styles} className='JGDashboardNewContentWrapper'>
                                <p className='JGDashboardNewContent'>
                                    {props.content}
                                </p>
                            </section>
                        )}
                    </Transition>
                </section>
            </Link>
        </section>
    );
}