import React from 'react'
import PropTypes from 'prop-types'
import JGSectionOneHome from '../JGSectionOneHome'
import JGSectionTwoHome from '../JGSectionTwoHome'
import { JGPopupRouter } from '../JGPopup'
import JGLoginForm from '../JGLoginForm'
import JGSigninForm from '../JGSigninForm'
import { SessionContext } from '../../Context/SessionContext'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    withRouter
} from "react-router-dom";

class JGHome extends React.Component {

    static defaultProps = {
        toggle: 'none',
    }

    constructor(props) {
        super(props)
        this.loginPopupHeight = 281
        this.signInPopupHeight = 474
        this.popupWidth = 500
        this.loginUrl = '/login'
        this.signInUrl = '/signin'
        this.state = {
            scrollY: 0
        }
    }

    closePopup = (event) => {
        this.props.onClickClosePopup(event);
    }

    popupDidMount = () => {
        document.body.style.overflow = 'hidden';
        document.body.style.paddingRight = '16px';
        this.props.nav_main_wrapper_ref.current.style.paddingRight = '24px'
        this.props.logo_ref.current.style.marginLeft = '8px';
    }

    popupDidUnMount = () => {
        document.body.style.overflow = 'visible'
        document.body.style.paddingRight = '0px'
        this.props.nav_main_wrapper_ref.current.style.paddingRight = '0px'
        this.props.logo_ref.current.style.marginLeft = '0px';
    }

    loginProcess = (event, username, password) => {
        this.props.loginProcess(event, username, password)
    }


    render() {

        const [popupTitle, formPopup, popupHeight] = this.props.toggle === 'Login' ?
            [
                'Login',
                <JGLoginForm errMsg={this.props.loginErrMsg} loginProcess={this.loginProcess} />,
                this.loginPopupHeight
            ] :
            [
                'Sign in',
                <JGSigninForm />,
                this.signInPopupHeight
            ]


        return (
            <React.Fragment>
                <Switch>
                    <Route exact path={[this.loginUrl, this.signInUrl]}>
                        {
                            <JGPopupRouter
                                width={this.popupWidth}
                                height={popupHeight}
                                title={popupTitle}
                                didMount={this.popupDidMount}
                                didUnMount={this.popupDidUnMount}
                                onClickClosePopup={this.closePopup}
                                link='/'
                            >
                                {formPopup}
                            </JGPopupRouter>
                        }
                    </Route>
                </Switch>
                <JGSectionOneHome />
                <JGSectionTwoHome />
            </React.Fragment>
        );
    }
}

JGHome.propTypes = {
    toggle: PropTypes.string.isRequired,
    onClickClosePopup: PropTypes.func.isRequired
}

export default JGHome

export const JGHomeRouter = withRouter(JGHome)

