import React from 'react'
import PropTypes from 'prop-types'

import './JGInput.css'

class JGInput extends React.Component {

    static defaultProps = {
        containterClassName: '',
        className: '',
        type: '',
        placeholder: '',
        value: '',
        name: '',
        label: '',
        errMsg: '',
        onChange: () => {
            //unit testing 
        }
    }

    handleOnChange = (event) => {
        this.props.onChange(event)
    }
    
    render() {
        return (
            <div className={this.props.containterClassName}>
                {this.props.errMsg != '' && 
                <p className='JGInputErr'>{this.props.errMsg}</p>}
                <label>{this.props.label}</label>
                <input 
                    className={this.props.className} 
                    type={this.props.type} 
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                    onChange={this.handleOnChange}
                    name={this.props.name}
                />
            </div>
        );
    }
}

JGInput.propTypes = {
    containterClassName: PropTypes.string,
    label: PropTypes.string.isRequired,
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    errMsg: PropTypes.string
}

export default JGInput