import React from 'react'
import './JGNotification.css'

function JGNotification(props) {
    return (
        <section className='JGNotificationWrapper'>
            <p className='JGNotificationDateWrapper'>18/10/2019</p>
            <div className='JGNotificationContentWrapper'>
                <div className='JGnotificationImgWrapper'><img className='JGNotificationImg'  src='/images/avatar.jpg' alt=''/></div>
                <div className='JGNotificationTextWrapper'>
                    <p className='JGNotificationDescWrapper'>Your rating has been adjusted by +25 as a cheater was caught in 1 matches you’ve played in</p>
                    <p className='JGNotificationHourWrapper'>12:00 AM</p>
                </div>
            </div>
        </section>
    );
}

export default JGNotification