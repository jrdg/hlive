import React from 'react';
import PropTypes from 'prop-types'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";


class JGButtonTogglePanel extends React.Component {

    static defaultProps = {
        idName: '',
        classStyle: '',
        text: ''
    }

    handleClick = (event) => {
        this.props.onClick(event)
    }

    render() {
        return (
            <Link to={this.props.link}>
                <button
                    id={this.props.idName}
                    className={`${this.props.classStyle} JGButtonTogglePanel`}
                    onClick={this.handleClick}
                >
                    {this.props.text !== '' ? this.props.text : this.props.children}
                </button>
            </Link>
        );
    }
}

JGButtonTogglePanel.propTypes = {
    idName: PropTypes.string.isRequired,
    classStyle: PropTypes.string,
    text: PropTypes.string
}

export default JGButtonTogglePanel
