import React, {useState, useEffect} from 'react'
import {useHistory, useParams} from 'react-router-dom'
import './JGNew.css'
import {news} from '../../Database/Db.js'

export default function JGNew(props) {

    let { newId } = useParams();
    let history = useHistory();
    const [current_new, setCurrent_new] = useState({})

    useEffect(() => {
        let find = false;
        for(let i = 0 ; i < news.length ; i++){
            if(news[i].id === parseInt(newId)){
                find = true
                setCurrent_new(news[i])
            }
        }
        if(!find){
            history.replace('/')
        }
    }, [])

    return (
        <section className='JGNewGlobalWrapper'>
            <h1 className='JGNewTitle'>BLAST PRO SERIES GLOBAL FINAL SCHEDULE REVEALED</h1>
            <h4 className='JGNewDescription'>RFRSH Entertainment has revealed the comprehensive schedule for the $500,000 BLAST Pro Series Global Final, which held in Riffa, Bahrain.</h4>
            <p className='JGNewTextBeforeImage'>Hosted at the ISA Sports City in Riffa, Bahrain, on December 12-14, the BLAST Pro Series Global Final will feature the four teams who secured the most BLAST Pro Series points over the course of the season, Astralis, Liquid, FaZe and NiP.</p>
            <p className='JGNewImage'><img src={current_new.imgSrc} /></p>
            <p className='JGNewTextAfterImage' >The quartet will be duking it out in a double-elimination, best-of-three bracket to determine the team walking away with the grand prize of $350,000 and the title of BLAST Pro Series Season Champion. RFRSH Entertainment has kept the iconic BLAST Pro Stand-off, in which two sides will face off in a series of 1v1 duels in looks to secure additional prize winnings.</p>
        </section>
    )
}