import React from 'react'
import './JGSidebar.css'
import JGSidebarSection from '../JGSidebarSection'
import { Spring, Transition } from 'react-spring/renderprops'
import {useState} from 'react'
import {withRouter} from 'react-router-dom'

export default function JGSidebar(props) {

    // All the map to display in the map list
    const maps = [
        {name: 'De_dust 2', mapUrl: '/map/dust2' },
        {name: 'De_mirage', mapUrl: '/map/mirage' },
        {name: 'De_nuke', mapUrl: '/map/nuke' },
        {name: 'De_inferno', mapUrl: '/map/inferno' },
        {name: 'De_vertigo' ,mapUrl: '/map/vertigo' },
        {name: 'De_train', mapUrl: '/map/train' },
        {name: 'De_overpass', mapUrl: '/map/overpass' }
    ]

    // key is the index at which the circle list decoration is gonna be red in the maps
    const [key, setKey] = useState(3)

    //function to change the key when the user click on a map list item
    function changeItem(index) {
        setKey(index)
    }

    return (
        <Spring
            from={{ left: "-300px" }}
            to={{ left: "0px" }}
        >
            {styles =>
                <nav id="JGSidebar" style={styles}>
                    <ul>
                        <JGSidebarSection singleButton link='/' title='DASHBOARD' />
                        {/*<JGSidebarSection title='CUSTOM BUILD'>
                            <JGSidebarSection singleButton notSection title='Create a new build' />
                        </JGSidebarSection>*/}
                        <JGSidebarSection title='MAP'>
                            {maps.map( (map, index) => 
                                <JGSidebarSection 
                                    active={key === index && props.history.location.pathname === map.mapUrl}
                                    link={{
                                        pathname: map.mapUrl,
                                        state: {justRefresh: false}
                                    }}
                                    key={index}
                                    singleButton
                                    notSection 
                                    title={map.name}
                                    onClick={() => {
                                        changeItem(index)
                                    }}
                                />
                            )}
                        </JGSidebarSection>
                    </ul>
                </nav>
            }
        </Spring>
    );
}

export const JGSidebarRouter = withRouter(JGSidebar)