import React, {useEffect, useState} from 'react'
import './JGVideoList.css'
import JGPreVideoSection from '../JGPreVideoSection'
import {useTransition, animated} from 'react-spring'
import {useLocation} from 'react-router-dom'
import {Transition} from 'react-spring/renderprops'


export default function JGVideoList(props) {

    let location = useLocation()
    const [justRefresh, setJustRefresh] = useState(false)
    const [numberVideoSection, setnumberVideoSection] = useState(0)

    function onClickOnVideo(event, current_video){
        props.onClickOnVideo(event, current_video)
    }

    const list = props.list

    useEffect(() => {
        if(location.state != undefined && 
            location.state.justRefresh){
             setJustRefresh(true)
        }
    }, [])

    function calculateNumberVideoSection() {
        if(justRefresh){
            setnumberVideoSection(numberVideoSection + 1)
        }
    }

    useEffect(() => {
        if(numberVideoSection == list.length){
            setJustRefresh(false)
        }
    }, [numberVideoSection])

    const fromStyle = {
        height: 0, 
        opacity: 0, 
        backgroundColor: 'red'
    }

    const enterStyle = {
        height: 'auto', 
        opacity: 1, 
        backgroundColor: '#171717'
    }

    const leaveStyle = {
        height: 0, 
        opacity : 0,
        backgroundColor: 'red'
    }

    return (
        <section className='JGVideoListRightSectionWrapper'>
            <h3 className='JGVideoListTitle'>{props.title}</h3>
            <section className='JGVideoListPreVideoSectionsWrapper'>
                {
                    props.list.length === 0 && 
                    <section id="JGVideoListChooseDotWrapper">
                        <p>Please choose a dot spot</p>
                    </section>
                }
                <Transition
                    onRest={() => calculateNumberVideoSection()}
                    immediate={() => justRefresh}
                    items={props.list} 
                    keys={item => item.id}
                    from={fromStyle}
                    enter={enterStyle}
                    leave={leaveStyle}>
                    {item => props =>              
                        <animated.div style={props}>
                           <JGPreVideoSection list={list} onClickOnVideo={onClickOnVideo} video={item} />
                        </animated.div>
                    }
                </Transition>
            </section>
        </section>
    );
}