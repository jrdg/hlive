import React from 'react'
import JGInput from '../JGInput'
import JGButtonFullWidth from '../JGButtonFullWidth'
import PropTypes from 'prop-types'


class JGLoginForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            username: '',
            password: '',
        }
    }

    handleOnChange = (event) =>{
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (event) => {
        this.props.loginProcess(
            event, 
            this.state.username, 
            this.state.password
        );
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} method="post">
                <JGInput 
                    type="text"  
                    name='username'
                    placeholder="Username"  
                    value={this.state.username}
                    label="USERNAME" 
                    className='popup_input_text' 
                    containterClassName='popup_input_label_container'
                    onChange={this.handleOnChange}
                    errMsg={this.props.errMsg}
                />
                <JGInput 
                    type="password"  
                    name='password'
                    placeholder="Enter your password" 
                    value={this.state.password}
                    label="PASSWORD" 
                    className='popup_input_text' 
                    containterClassName='popup_input_label_container'
                    onChange={this.handleOnChange}
                />
                <JGButtonFullWidth 
                    text='Log in' 
                    className='popup_home_page_button'
                />
            </form>
        );
    }
}

JGLoginForm.propTypes = {
    errMsg: PropTypes.string
}

export default JGLoginForm