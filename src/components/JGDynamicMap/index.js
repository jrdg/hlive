import React, {useState, useEffect} from 'react'
import './JGDynamicMap.css'

export default function JGDynamicMap(props) {

    return (
        <section className='JGDynamicMapWrapper' >
            <img className='JGDynamicMap' src={props.mapUrl}></img>
            {props.children}
        </section>
    );
}